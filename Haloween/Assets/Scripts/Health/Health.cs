﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using System;

[RequireComponent (typeof (Animator))]
[RequireComponent(typeof(Collider2D))]
[RequireComponent(typeof(SpriteRenderer))]
public abstract class Health : MonoBehaviour 
{
    [Header("Health")]
    public int healthPoint;
    public int healthPointMax;

    public bool isDestroyable;
    public GameObject destroyedUnit;
    [SerializeField]
    protected float destroyTime = 3f;

    [Header("Enemy Info")]
    public string enemyTag;
    public LayerMask enemyLayer;

    #region Delegates
    public UnityAction characterDie;
    public UnityAction<int> checkHealthDelegate;

    public UnityAction<GameObject> takeDamageDelegate;
    public UnityAction<GameObject> takeHealDelegate;
    #endregion

    protected SpriteRenderer spriteRenderer;
    protected Animator animator;
    [HideInInspector]
    public Collider characterColl;


    #region Properties
    public float HealthPercents
    {
        get
        {
            return healthPointMax != 0.0f ? (healthPoint / healthPointMax) : 0.0f ;
        }
    }
    public bool IsAllive
    {
        get
        {
            return healthPoint > 0.0f;
        }
    }
    #endregion Properties

    protected virtual void Start()
    {       
        animator = GetComponent<Animator>();
        characterColl = GetComponent<Collider>();
        spriteRenderer = GetComponent<SpriteRenderer>();
    }

  

    public virtual void InitTag(string enemyTag,int enemyLayer)
    {
        // enemyTag = gameObject.tag == Tags.Enemy ? Tags.Player : Tags.Enemy;
       // enemyLayer = gameObject.tag == Tags.Enemy ? Layers.PLAYER_LAYER : Layers.ENEMY_LAYER;
        this.enemyTag = enemyTag;
        this.enemyLayer = enemyLayer;
    }
    /// <summary>
    /// return true is character is alive, return false => character die
    /// </summary>
    /// <returns></returns>
    public virtual void CheckHealth()
    {
        checkHealthDelegate?.Invoke(healthPoint);
        if (healthPoint <= 0)
        {
            //AudioHelper.Instance.PlayEffect(AudioClips.DIVER_DEATH);
            MyDestroy();
        }
    }

    public virtual void TakeHeal(int heal, GameObject fromWho)
    {
        takeHealDelegate?.Invoke(fromWho);
        CheckHealth();
    }

    public virtual void TakeDamage(int damage, GameObject fromWho)
    {
        if (Common.IsPause) return;

        if (!isDestroyable) return;

        healthPoint -= damage;
        takeDamageDelegate?.Invoke(fromWho);
        CheckHealth();
    }

    public virtual void TakeDamage()
    {
        if (!isDestroyable)
            return;

        healthPoint = 0;
        CheckHealth();
    }

    protected virtual void SpawnDestroyParticles()
    {
        if (destroyedUnit != null)
        {
            GameObject go = Instantiate(destroyedUnit, transform.position, transform.rotation);
            go.transform.localScale = transform.localScale;
        }
    }

    public virtual void MyDestroy()
    {  
        SpawnDestroyParticles();
        characterDie?.Invoke();
    }

    public virtual void SelfDestroy()
    {
        Destroy(gameObject,destroyTime);
    }

    #region COLLIDERS
    protected virtual void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == Tags.DeadTrigger)
        {
            TakeDamage();
            return;
        }
    }

    protected virtual void OnCollisionEnter2D(Collision2D coll)
    {

    }
    #endregion
}
