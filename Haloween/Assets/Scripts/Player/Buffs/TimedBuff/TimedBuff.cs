using UnityEngine;

public abstract class TimedBuff
{
    protected float DurationStart;
    protected float Duration;
    protected int EffectStacks;
    public ScriptableBuff Buff { get; }
    protected readonly GameObject Obj;

    protected  GameObject buffSpriteObj;
    protected BuffUICounter buffUICounter;

    public bool IsFinished;

    public TimedBuff(ScriptableBuff buff, GameObject obj)
    {
        Buff = buff;
        Obj = obj;
    }

    public void Tick(float delta)
    {
        Duration -= delta;
        UpdateBuffUIDuration();
        if (Duration <= 0)
        {
            End();
            RemoveBuffFromUI();
            IsFinished = true;
        }
    }

    /**
     * Activates buff or extends duration if ScriptableBuff has IsDurationStacked or IsEffectStacked set to true.
     */
    public void Activate(HealthBar healthBar)
    {
        if (Buff.IsEffectStacked || Duration <= 0)
        {
            ApplyEffect();
            AddBuffToUI(healthBar);
            EffectStacks++;
        }
        
        if (Buff.IsDurationStacked || Duration <= 0)
        {
            Duration += Buff.Duration;
            DurationStart = Buff.Duration;
        }
    }
    protected abstract void ApplyEffect();
    public abstract void End();

    protected void AddBuffToUI(HealthBar healthBar) 
    {
        buffSpriteObj = healthBar.AddBuff(Buff.spriteBuff);
        buffUICounter = buffSpriteObj.GetComponent<BuffUICounter>();
    }
    public void RemoveBuffFromUI()
    {
        if (buffSpriteObj != null)
            GameObject.Destroy(buffSpriteObj);
    }

    private void UpdateBuffUIDuration()
    {
        buffUICounter.UpdateDuration(Duration, DurationStart);
    }
}
