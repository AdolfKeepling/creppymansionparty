using ScriptableObjects;
using UnityEngine;

public class TimedMoveSpeedBuff : TimedBuff
{
    private readonly PlatformerCharacter2D _movementComponent;

    public TimedMoveSpeedBuff(ScriptableBuff buff, GameObject obj) : base(buff, obj)
    {
        //Getting MovementComponent, replace with your own implementation
        _movementComponent = obj.GetComponent<PlatformerCharacter2D>();
    }

    protected override void ApplyEffect()
    {
        //Add speed increase to MovementComponent
        ScriptableMoveSpeedBuff speedBuff = (ScriptableMoveSpeedBuff) Buff;
        _movementComponent.moveSpeed += speedBuff.SpeedIncrease;
    }

    public override void End()
    {
        //Revert speed increase
        ScriptableMoveSpeedBuff speedBuff = (ScriptableMoveSpeedBuff) Buff;
        _movementComponent.moveSpeed -= speedBuff.SpeedIncrease * EffectStacks;
        EffectStacks = 0;
    }
}
