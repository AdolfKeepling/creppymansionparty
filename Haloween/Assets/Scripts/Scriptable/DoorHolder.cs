﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Data", menuName = "ScriptableObjects/SpawnManagerScriptableObject", order = 1)]
public class DoorHolder : ScriptableObject
{
    public List<DoorType> doorTypes = new List<DoorType>();
}
