﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Settings
{
    public static int MinVolume { get; set; } = 0;
    public static int MaxVolume { get; set; } = 100;
    public static int IncVolume { get; set; } = 10;

    private static int sound = 50;
    public static int Sound
    {
        get => sound;
        set
        {
            sound = value;
            if (sound <= MinVolume)
                sound = MinVolume;
            if (sound >= MaxVolume)
                sound = MaxVolume;
        }
    }

    private static int music = 50;
    public static int Music
    {
        get => music;
        set
        {
            music = value;
            if (music <= MinVolume)
                music = MinVolume;
            if (music >= MaxVolume)
                music = MaxVolume;
        }
    }

    public static List<string> langList = new List<string>()
    { "en","ru","ua" };

    public static int LanguageIndex
    {
        get; set;
    } = 0;



    public static string GetCurrentLang
    {
        get { return langList[LanguageIndex]; }
    }
}
