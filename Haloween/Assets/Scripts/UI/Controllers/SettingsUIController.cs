﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SettingsUIController : AbstractUIController
{
    SelectorController selector;

    [SerializeField]
    SelectorSlider soundSlider;
    [SerializeField]
    SelectorSlider musicSlider;
    [SerializeField]
    SelectorSlider langSlider;

    private void Start()
    {
        selector = GetComponentInChildren<SelectorController>();
        UpdateAudio(soundSlider, Settings.Sound, Settings.MinVolume, Settings.MaxVolume);
        UpdateAudio(musicSlider, Settings.Music, Settings.MinVolume, Settings.MaxVolume);
        UpdateSlider(langSlider, Settings.GetCurrentLang, 1);
    }

    protected void Up()
    {
        selector.MoveUp();
    }
    protected void Down()
    {
        selector.MoveDown();
    }
    protected void Right()
    {
        selector.MoveRight();
    }
    protected void Left()
    {
        selector.MoveLeft();
    }
    protected override void CreateDictionary()
    {
        inputHolder.Add(InputKeys.EXIT, Exit);
        inputHolder.Add(InputKeys.UP, Up);
        inputHolder.Add(InputKeys.DOWN, Down);
        inputHolder.Add(InputKeys.RIGHT, Right);
        inputHolder.Add(InputKeys.LEFT, Left);
    }
    protected override void DestroyDictionary()
    {
        inputHolder.Clear();
    }

    private void UpdateAudio(SelectorSlider slider, int currentValue, int min,int max)
    {
        int state = GetArrowState(currentValue, min, max);
        UpdateSlider(slider, currentValue.ToString(), state);
    }

    /// <summary>
    /// Update slider view. 
    /// </summary>
    /// <param name="slider"></param>
    /// <param name="currentValue"></param>
    /// <param name="arrowState"> 0 - disable left , 1 - enable both, 2 - disable right</param>
    private void UpdateSlider(SelectorSlider slider , string currentValue,int arrowState )
    {
        slider.ValueTxt.text = currentValue;
        slider.ArrowRightImg.enabled = arrowState != 2;
        slider.ArrowLeftImg.enabled = arrowState != 0;
    }

    private int GetArrowState(int value , int min, int max)
    {
        if (value == min)
            return 0;
        else if (value == max)
            return 2;
        else 
            return 1;
    }

    #region Settings Functions
    /// <summary>
    /// Exit to pause menu
    /// </summary>
    public void Exit()
    {
        EventManager.SendEvent(EventKeys.UPDATE_UI_STATE, UIAppStates.pause);
    }
    public void IncreaseSound()
    {
        Settings.Sound += Settings.IncVolume;
        UpdateAudio(soundSlider, Settings.Sound, Settings.MinVolume, Settings.MaxVolume);
    }
    public void DecreaseSound()
    {
        Settings.Sound -= Settings.IncVolume;
        UpdateAudio(soundSlider, Settings.Sound, Settings.MinVolume, Settings.MaxVolume);
    }
    public void IncreaseMusic()
    {
        Settings.Music += Settings.IncVolume;
        UpdateAudio(musicSlider, Settings.Music, Settings.MinVolume, Settings.MaxVolume);
    }
    public void DecreaseMusic()
    {
        Settings.Music -= Settings.IncVolume;
        UpdateAudio(musicSlider, Settings.Music, Settings.MinVolume, Settings.MaxVolume);
    }
    public void IncreaseLang()
    {
        Settings.LanguageIndex = Common.UpdateSelectedIndexInList(Settings.LanguageIndex,Settings.langList.Count,true);
        UpdateSlider(langSlider, Settings.GetCurrentLang, 1 );
    }
    public void DecreaseLang()
    {
        Settings.LanguageIndex = Common.UpdateSelectedIndexInList(Settings.LanguageIndex, Settings.langList.Count, false);
        UpdateSlider(langSlider, Settings.GetCurrentLang, 1 );
    }

    #endregion
}
