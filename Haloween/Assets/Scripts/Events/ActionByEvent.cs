﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UniRx;
using System.Linq;


[System.Serializable]
public class ActionEvent
{
    public string eventKey;
    public UnityEvent eventAction;

    public ActionEvent()
    {
        eventKey = "";
        eventAction = null;
    }
}

public class ActionByEvent : AbstractByEvent
{

    public List<ActionEvent> actionEvents;

    private ActionEvent _actionEvent;

    #region EVENT
    public CompositeDisposable changeAppState;
    void OnEnable()
    {
        changeAppState = new CompositeDisposable();
        MessageBroker.Default
            .Receive<EventManager>()
            .Where(msg => ContaintsEvent(msg.key))
            .Subscribe(msg => {
                print("RECEIVE ACTION " + msg.key);
                if (_actionEvent != null)
                    _actionEvent.eventAction.Invoke();
                _actionEvent = null;

            }).AddTo(changeAppState);
    }

    void OnDisable()
    {
        if (changeAppState != null)
        {
            changeAppState.Dispose();
        }
    }
    #endregion

    private void Start()
    {
        _actionEvent = new ActionEvent();
    }

    protected bool ContaintsEvent(string key)
    {
        if (actionEvents.Count == 0) return false;

        bool value = false;

        foreach (var x in actionEvents)
        {
            if (x.eventKey == key)
            {
                _actionEvent = x;
                value = true;
                break;
            }
        }
        return value;
    }

   
}
