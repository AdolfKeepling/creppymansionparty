using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class BuffableEntity: MonoBehaviour
{
    private readonly Dictionary<ScriptableBuff, TimedBuff> _buffs = new Dictionary<ScriptableBuff, TimedBuff>();

    public HealthBar HealthBar { get; set; }

    void Update()
    {
        //OPTIONAL, return before updating each buff if game is paused
        if (Common.IsPause)
            return;

        foreach (var buff in _buffs.Values.ToList())
        {
            buff.Tick(Time.deltaTime);
            if (buff.IsFinished)
            {
                _buffs.Remove(buff.Buff);
            }
        }
    }
    public void AddBuff(TimedBuff buff)
    {
        if (_buffs.ContainsKey(buff.Buff))
        {
            _buffs[buff.Buff].Activate(HealthBar);
        }
        else
        {
            _buffs.Add(buff.Buff, buff);
            buff.Activate(HealthBar);
        }
    }
}
