﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BuffUICounter : MonoBehaviour
{
    [SerializeField] Image buffDurationImg;


    public void UpdateDuration(float current, float start)
    {
        buffDurationImg.fillAmount = 1 - current / start;
    }
}
