﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RangeAttack : CharacterAttackAbstract
{
    public Transform bulletSpawnPosition;
    public GameObject bulletRangePrefab;


    protected override void AttackWeapon()
    {
        RangeWeaponAttack();
    }

    protected virtual void RangeWeaponAttack()
    {
        shotDelegate?.Invoke();

        Shot();
        SendAttackSound();
    }

    protected virtual void Shot()
    {
        GameObject bullet =
            Instantiate(bulletRangePrefab, bulletSpawnPosition.position, Quaternion.identity);

        bullet.GetComponent<AbstractBulletController>().InitBullet(damage, splashAttackType, raycastSplashRadius);

        if (transform.localScale.x > 0)
            bullet.transform.localScale = new Vector3(-1 * bullet.transform.localScale.x, bullet.transform.localScale.y, bullet.transform.localScale.z);
    }
}
