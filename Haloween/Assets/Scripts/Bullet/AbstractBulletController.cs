﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public abstract class AbstractBulletController : MonoBehaviour {


    protected Rigidbody2D m_RigBd;
    protected Health m_Health;
    protected Collider2D m_Collider; 
    [SerializeField]
    public int damage;
    [SerializeField]
    protected float speed;

    [SerializeField]
    public MeleeAttackType bulletAttackType;
    public float raycastSplashRadius;

    public UnityAction<GameObject> targetAttackSomeOne;
    public UnityAction<GameObject> splashAttackSomeOne;
    public string soundKey;


    private bool _isMoevement = false;
    public virtual bool IsMovement
    {
        get { return _isMoevement; }
        set
        {
            _isMoevement = value;
        }
    }

    protected void Start () {
        m_RigBd = GetComponent<Rigidbody2D>();
        m_Collider = GetComponent<Collider2D>();
        m_Health = GetComponent<Health>();

        SpawnSound();
    }
	
	protected void Update ()
    {
        if (Common.IsPause) return;
        Movement();
    }

    protected void OnBecameInvisible()
    {
        Destroy(gameObject);
    }

    protected void SpawnSound()
    {
       // EventManager.SendEvent(soundKey);
    }

    public virtual void InitBullet(int dmg, MeleeAttackType bulletAttackType = MeleeAttackType.target,float splashRadius = 0, Transform _target = null )
    {
        IsMovement = true;
        damage = dmg;
        this.bulletAttackType = bulletAttackType;
        raycastSplashRadius = splashRadius;
    }

    protected abstract void Movement();


    #region COLLIDERS
    protected virtual void OnCollisionEnter2D(Collision2D coll)
    {
        DetectCollision(coll.gameObject);
    }
    #endregion

    protected virtual void DetectCollision(GameObject coll)
    {
        print(coll.name);
        if (coll.GetComponent<Health>() == null)
        {
            m_Health.TakeDamage();
            return;
        }

        Health collHealth = coll.GetComponent<Health>();
        if (collHealth.tag == m_Health.enemyTag)
        {
            switch (bulletAttackType)
            {
                case MeleeAttackType.splash: { SplashAttack(); break; }
                case MeleeAttackType.target: { TargetAttack(collHealth); break; }
                case MeleeAttackType.mixed: { TargetAttack(collHealth); SplashAttack(); break; }
            }
            m_Health.TakeDamage();
        }       
    }

    protected virtual void TargetAttack(Health collHealth)
    {
        if (collHealth != null)
        {
            targetAttackSomeOne?.Invoke(collHealth.gameObject);
            collHealth.TakeDamage(damage, gameObject);
        }
    }

    protected void SplashAttack()
    {
        Collider[] meleeHitColl = 
        Physics.OverlapSphere(transform.position, raycastSplashRadius, m_Health.enemyLayer.value);
        print("Splash bullet ATTACK " + meleeHitColl.Length);

        foreach (Collider col in meleeHitColl)
        {
            if (col != null)
            {
                print("TRY TO TAKE DAMAGE ");
                splashAttackSomeOne?.Invoke(col.gameObject);
                col.gameObject.GetComponent<Health>().TakeDamage(damage, gameObject);
            }
        }
    }
}
