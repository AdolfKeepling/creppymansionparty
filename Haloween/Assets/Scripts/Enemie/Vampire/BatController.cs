﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BatController : MonoBehaviour
{
    [SerializeField]
    private List<Transform> m_movePoints = new List<Transform>();
    public Transform movePointsParent;
    public float speed;
    private int m_index = 0;
    private float m_distToPoint = 0.05f;
    protected Health m_Health;
    public int damage;

    protected void Start()
    {
        m_Health = GetComponent<Health>();
        GetMovePoints();
    }

    private void Update()
    {
        if (Common.IsPause) return;

        WalkOnMovePoints();
    }

    protected virtual void GetMovePoints()
    {
        for (int i = 0; i < movePointsParent.childCount; i++)
            m_movePoints.Add(movePointsParent.GetChild(i));
    }

    private void FollowObject(GameObject _obj)
    {      
        if (_obj != null)
        {
            transform.position = Vector3.MoveTowards(transform.position, _obj.transform.position, speed * Time.deltaTime);  
        }
    }

    protected void WalkOnMovePoints()
    {
        if (Vector3.Distance(transform.position , m_movePoints[m_index].position) < m_distToPoint)
        {
            m_index = Common.UpdateSelectedIndexInList(m_index, m_movePoints.Count, true);
            LookAtTarget(m_movePoints[m_index]);
        }
        FollowObject(m_movePoints[m_index].gameObject);
    }

    protected void LookAtTarget(Transform _value)
    {
        if (_value.position.x > transform.position.x)
        {
            if (transform.localScale.x > 0)
                Flip();
        }
        else
        if (_value.position.x < transform.position.x)
        {
            if (transform.localScale.x < 0)
                Flip();
        }
    }

    private void Flip()
    {
        // Multiply the player's x local scale by -1.
        Vector3 theScale = transform.localScale;
        theScale.x *= -1;
        transform.localScale = theScale;
    }

    protected virtual void OnTriggerEnter2D(Collider2D _other)
    {
        DetectCollision(_other.gameObject);
    }

    protected virtual void DetectCollision(GameObject _coll)
    {
        Health _collHealth = _coll.GetComponent<Health>();

        if (_collHealth == null)
        {
            return;
        }

        if (_collHealth.tag == m_Health.enemyTag)
        {
            _collHealth.TakeDamage(damage, gameObject);
        }
    }
}
