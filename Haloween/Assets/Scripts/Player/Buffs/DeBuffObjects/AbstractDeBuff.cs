﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AbstractBulletController))]
public abstract class AbstractDeBuff :MonoBehaviour
{

    [SerializeField]
    protected ScriptableBuff scriptableBuff;
    private AbstractBulletController bulletController;

    protected void OnEnable()
    {
        bulletController = GetComponent<AbstractBulletController>();
        bulletController.targetAttackSomeOne += DoDebuff;
    }

    protected void OnDisable()
    {
        bulletController.targetAttackSomeOne -= DoDebuff;
    }

    public abstract void DoDebuff(GameObject target);
    
}
