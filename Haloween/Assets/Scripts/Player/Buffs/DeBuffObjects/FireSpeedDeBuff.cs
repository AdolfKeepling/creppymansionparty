﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireSpeedDeBuff : AbstractDeBuff
{
    public override void DoDebuff(GameObject target)
    {
        TimedBuff speedBuff = new TimedFireSpeedBuff(scriptableBuff, target);
        target.GetComponent<BuffableEntity>().AddBuff(speedBuff);
    }
}