﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealBuff : AbstractUsableItemsController, IBuff
{
    // RED SCRIPT
    [SerializeField] int healAmount = 1;
    public float DestroyTime { get; set; } = 10f;

    protected new void Start()
    {
        base.Start();
        SelfDestroy();
    }

    public void SelfDestroy()
    {
        Destroy(gameObject,DestroyTime);
    }

    protected override void ActionInput(GameObject player)
    {
        if (!isVisible) return;

        if (Vector2.Distance(player.transform.position, transform.position) < distToPlayer)
        {
            player.GetComponent<Health>().TakeHeal(healAmount, gameObject);
            Destroy(gameObject);
        }
    }
}
