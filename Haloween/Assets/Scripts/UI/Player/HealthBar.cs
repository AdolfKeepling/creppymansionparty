﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthBar : MonoBehaviour
{
    [SerializeField]
    private List<Animator> m_hpAnimUI = new List<Animator>();
    [SerializeField]
    private Health m_healthCharacter;
    private string m_deathKey = "Death";

    [SerializeField] GameObject healthBarPrefab;
    [SerializeField] Transform healthBarParent;
    [SerializeField] Transform buffBarParent;
    [SerializeField] GameObject buffBarPrefab;

    private void OnDestroy()
    {
        m_healthCharacter.checkHealthDelegate -= UpdateHealth;
    }

    public void InitHelathBar(GameObject character)
    {
        m_healthCharacter = character.GetComponent<Health>();
        m_healthCharacter.checkHealthDelegate += UpdateHealth;

        for (int i = 0; i < m_healthCharacter.healthPoint; i++)
        {
            AddHP();
        }
    }

    private void UpdateHealth(int value)
    {
        if (value < m_hpAnimUI.Count)
            RemoveHP(value);
        if (value > m_hpAnimUI.Count)
            AddHP(value);
    }

    private void AddHP()
    {
        GameObject hpBar = Instantiate(healthBarPrefab, healthBarParent);
        m_hpAnimUI.Add(hpBar.GetComponent<Animator>());
    }

    private void AddHP(int value)
    {
        int x = value - m_hpAnimUI.Count;
        for (int i = 0; i < x; i++)
        {
            AddHP();
        }
    }
    private void RemoveHP()
    {
        m_hpAnimUI[m_hpAnimUI.Count - 1].SetTrigger(m_deathKey);
        m_hpAnimUI.RemoveAt(m_hpAnimUI.Count - 1);
    }

    private void RemoveHP(int value)
    {
        if (value < m_hpAnimUI.Count)
        {
            int x = m_hpAnimUI.Count - value;
            for (int i = 0; i < x; i++)
            {
                RemoveHP();
            }
        }
    }

    public GameObject AddBuff(Sprite sprite)
    {
        GameObject bf = Instantiate(buffBarPrefab,buffBarParent);
        bf.GetComponent<Image>().sprite = sprite;
        return bf;
    }
}
