﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(SpriteRenderer))]
[RequireComponent(typeof(BoxCollider2D))]
public class DoorController : AbstractUsableItemsController
{
    private BoxCollider2D m_boxCollider;
    public ItemColor doorColor;

    [SerializeField]
    private bool isLocked;
    public bool IsLocked
    {
        get => isLocked;
        set
        {
            isLocked = value;
        }
    }
  
    public override bool IsOpened
    {
        get => isOpened;
        set
        {
            isOpened = value;
            SetDoorState();
        }
    }

    private bool isVisisble = false;

    [SerializeField]
    private DoorType doorType;

    protected new void Start()
    {
        base.Start();
        m_boxCollider = GetComponent<BoxCollider2D>();
        GetDoorType();
        IsOpened = false;
        distToPlayer = 0.3f;
    }

    private void GetDoorType()
    {
        doorType = ScriptableObjHolder.instance.GetDoorType(doorColor);
    }

    protected override void ActionInput(GameObject player)
    {
        if (Vector2.Distance(player.transform.position, transform.position) < distToPlayer)
        {
            if (IsLocked)
            {
                return;
            }
            IsOpened = true;
        }
    }

    protected void SetDoorState()
    {
        m_spriteRenderer.sprite = IsOpened? doorType.openedDoor:doorType.closerDoor;
        m_boxCollider.isTrigger = IsOpened;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.GetComponent<PlayerController>() != null)
        {

        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.GetComponent<PlayerController>() != null)
        {
            IsOpened = false;
        }
    }
}
