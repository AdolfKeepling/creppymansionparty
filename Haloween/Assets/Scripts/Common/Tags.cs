// ----- AUTO GENERATED CODE ----- //
public static class Tags
{
	public static readonly string Untagged = "Untagged";
	public static readonly string Respawn = "Respawn";
	public static readonly string Finish = "Finish";
	public static readonly string EditorOnly = "EditorOnly";
	public static readonly string MainCamera = "MainCamera";
	public static readonly string Player = "Player";
	public static readonly string GameController = "GameController";
	public static readonly string Helper = "Helper";
	public static readonly string Light = "Light";
	public static readonly string Ground = "Ground";
	public static readonly string Enemy = "Enemy";
	public static readonly string BagItem = "BagItem";
	public static readonly string Children = "Children";
	public static readonly string Wall = "Wall";
	public static readonly string Door = "Door";
	public static readonly string DeadTrigger = "DeadTrigger";
	public static readonly string hpUIHolder = "hpUIHolder";
}
