﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthCharacter : Health
{
    private Color baseColor;
    private Color damageColor = new Color(1, 1, 1, 0);

    protected override void Start()
    {
        base.Start();
        baseColor = spriteRenderer.color;
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.P))
        {
            TakeDamage(1, gameObject);
        }
        if (Input.GetKeyDown(KeyCode.O))
        {
            TakeHeal(1,gameObject);
        }
    }

    public override void TakeDamage(int damage, GameObject fromWho)
    {
        base.TakeDamage(damage, fromWho);
        if (!IsAllive)
            return;
        StartCoroutine(TakeDamageAnimation());
    }

    public override void MyDestroy()
    {
        base.MyDestroy();
        EventManager.SendEvent(EventKeys.PLAYER_DIED);
        gameObject.SetActive(false);
        //Destroy(gameObject);
    }

    public override void TakeHeal(int heal, GameObject fromWho)
    { 
        healthPoint += (healthPoint + heal <= healthPointMax) ? heal : 0;
        base.TakeHeal(heal, fromWho);
    }

    private IEnumerator TakeDamageAnimation()
    {
        if (isDestroyable)
        {
            isDestroyable = false;
            for (int i = 0; i < 4; i++)
            {
                spriteRenderer.color = i % 2 == 0 ? damageColor : baseColor; 
                yield return new WaitForSeconds(0.15f);
            }
            isDestroyable = true;
        }
    }
}
