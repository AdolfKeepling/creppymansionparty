﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using System;
using UniRx;

public class GameUIController : AbstractUIController
{

    protected void ShowPause()
    {
        print("ShowPause");
        EventManager.SendEvent(EventKeys.UPDATE_UI_STATE, UIAppStates.pause);
    }

    protected override void CreateDictionary()
    {
        inputHolder.Add(InputKeys.EXIT, ShowPause);
    }

    protected override void DestroyDictionary()
    {
        inputHolder.Clear();
    }
}
