﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputKeys
{

    public const string UP = "Up";
    public const string DOWN = "Down";
    public const string LEFT = "Left";
    public const string RIGHT = "Right";

    public const string ACTION = "Action";
    public const string SUBMIT = "Submit";
    public const string EXIT = "Exit";
    public const string RESTART = "Restart";
}
