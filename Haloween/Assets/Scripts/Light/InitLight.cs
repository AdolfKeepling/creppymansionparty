﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InitLight : MonoBehaviour {

    MeshRenderer meshRenderer;

   
    protected void OnEnable()
    {
       
    }
    protected void OnDisable()
    {
       
    }

    void Awake()
    {
        meshRenderer = GetComponent<MeshRenderer>();
    }

    void Start () {
        CheckLightSource();

    }

    protected void UpdateLightSource(bool value)
    {
        meshRenderer.enabled = value;
    }

    protected void CheckLightSource()
    {
        bool value = LightController.IsLightLevel;
        UpdateLightSource(value);
    }
}
