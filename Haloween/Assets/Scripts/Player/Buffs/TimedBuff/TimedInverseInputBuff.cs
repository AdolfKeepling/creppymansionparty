﻿using System.Collections;
using System.Collections.Generic;
using ScriptableObjects;
using UnityEngine;

public class TimedInverseInputBuff : TimedBuff
{
    private readonly PlayerController _playerComponent;

    public TimedInverseInputBuff(ScriptableBuff buff, GameObject obj) : base(buff, obj)
    {
        //Getting MovementComponent, replace with your own implementation
        _playerComponent = obj.GetComponent<PlayerController>();
    }

    protected override void ApplyEffect()
    {
        _playerComponent.DirectUnSignInput();
        _playerComponent.InverseSignInput();
    }

    public override void End()
    {
        _playerComponent.InverseUnSignInput();
        _playerComponent.DirectSignInput();
    }
}