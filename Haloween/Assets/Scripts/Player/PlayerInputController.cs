﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInputController : MonoBehaviour
{
    public Action<float> horizontalInput;
    public Action<float> verticalInput;

    public Action jumpInput;
    public Action fireInput;

    public PlayerInput playerInput;


    public void InitInput(PlayerInput input)
    {
        playerInput = input;
    }

    void FixedUpdate()
    {
        if (Common.IsPause) return;

        GetAxis(playerInput.horizontal, horizontalInput);
        GetAxis(playerInput.vertical, verticalInput);
    }

    private void Update()
    {
        if (Common.IsPause) return;

        GetButton(playerInput.fire, fireInput);
        GetButton(playerInput.jump, jumpInput);

        GetButton(playerInput.action);
    }

    private void GetAxis(string value, Action<float> action)
    {
        action?.Invoke(Input.GetAxisRaw(value));
    }

    private void GetButton(string button, Action action)
    {
        if (Input.GetButtonDown(button))
        {
            action?.Invoke();
        }
    }

    private void GetButton(string button)
    {
        if (Input.GetButtonDown(button))
        {
            EventManager.SendEvent(EventKeys.PLAYER_DO_ACTION,gameObject);
        }
    }
}

[System.Serializable]
public class PlayerInput
{
    public string horizontal;
    public string vertical;
    public string fire;
    public string jump;
    public string action;

    public PlayerInput(string horizontal, string vertical, string fire, string jump, string action)
    {
        this.horizontal = horizontal;
        this.vertical = vertical;
        this.fire = fire;
        this.jump = jump;
        this.action = action;
    }

    public PlayerInput(PlayerInput playerInput)
    {
        horizontal = playerInput.horizontal;
        vertical = playerInput.vertical;
        fire = playerInput.fire;
        jump = playerInput.jump;
        action = playerInput.action;
    }
}
