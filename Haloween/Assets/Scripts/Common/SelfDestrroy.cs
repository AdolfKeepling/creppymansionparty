﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelfDestrroy : MonoBehaviour {

    public float selfDestroyTime;

    void Start () {  
        Destroy(gameObject, selfDestroyTime);
	}
}
