﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MachineGunAttack : RangeAttack
{

    [SerializeField]
    protected List<Transform> m_bulletSpawnPositions;

    protected virtual void GetShotPoints()
    {
        for (int i = 0; i < bulletSpawnPosition.childCount; i++)
            m_bulletSpawnPositions.Add(bulletSpawnPosition.GetChild(i));
    }

    protected new void Start()
    {
        base.Start();
        GetShotPoints();
    }

    public override void AnimationAttack()
    {
        AttackWeapon();
    }

    public void StopAttack()
    {
        IsAttackAvaliable = false;
        m_coolDownTimer = 0;
    }

    protected override void Shot()
    {
        GameObject bullet =
            Instantiate(bulletRangePrefab, RandomBulletSpawn.position, Quaternion.identity);

        bullet.GetComponent<AbstractBulletController>().InitBullet(damage, splashAttackType, raycastSplashRadius);

        if (transform.localScale.x > 0)
            bullet.transform.localScale = new Vector3(-1 * bullet.transform.localScale.x, bullet.transform.localScale.y, bullet.transform.localScale.z);
    }

    private Transform RandomBulletSpawn
    {
        get
        {
            int i = Random.Range(0, m_bulletSpawnPositions.Count);
            return m_bulletSpawnPositions[i];
        }
    }
}
