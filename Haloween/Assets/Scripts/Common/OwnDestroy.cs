﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OwnDestroy : MonoBehaviour
{
    public void SelfDestroy()
    {
        Destroy(gameObject);
    }
}
