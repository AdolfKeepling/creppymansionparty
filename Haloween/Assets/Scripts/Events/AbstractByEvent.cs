﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class AbstractByEvent : MonoBehaviour {

    public void SentEvent(string eventName)
    {
        EventManager.SendEvent(eventName);
    }
    public void SentEvent(string eventKey, string value)
    {
        EventManager.SendEvent(eventKey, value);
    }
   
    public void LoadLevelStr(string levelName)
    {
        EventManager.SendEvent(EventKeys.LOAD_LEVEL_NAME, levelName);
    }

    public void LoadLevelNum(int levelNum)
    {
        EventManager.SendEvent(EventKeys.LOAD_LEVEL_NUM, levelNum);
    }

    public void LoadNextLevel()
    {
        EventManager.SendEvent(EventKeys.LOAD_NEXT_LEVEL);
    }

    public void LoadNextLevelDelay(float delay)
    {
        EventManager.SendEvent(EventKeys.LOAD_NEXT_LEVEL_DELAY, delay);
    }

    public void UpdateUIPanel(int state)
    {
        EventManager.SendEvent(EventKeys.UPDATE_UI_STATE, (UIAppStates)state);
    }

    public void SetAchievement(string key)
    {
        SentEvent(EventKeys.ACTIVATE_ACHIEVEMENT, key);
    }
}
