﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class CollisionActionByEvent : AbstractByEvent
{
    public List<string> enterTag;
    public UnityEvent onCollisionEnter;
    [Space(20)]
    public List<string> exitTag;
    public UnityEvent onCollisionExit;

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (CheckTag(collision.gameObject.tag, enterTag))
        {
            print("On collision enter");
            onCollisionEnter.Invoke();
        }
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        if (CheckTag(collision.gameObject.tag,exitTag))
        {
            print("On collision exit");
            onCollisionExit.Invoke();
        }
    }


    private bool CheckTag(string value, List<string> list)
    {
        foreach (string x in list)
            if (x == value)
                return true;
        return false;
    }
}
