﻿using System.Collections;
using System.Collections.Generic;
using ScriptableObjects;
using UnityEngine;

public class TimedFireSpeedBuff : TimedBuff
{
    private readonly PlayerController _playerComponent;

    public TimedFireSpeedBuff(ScriptableBuff buff, GameObject obj) : base(buff, obj)
    {
        //Getting MovementComponent, replace with your own implementation
        _playerComponent = obj.GetComponent<PlayerController>();
    }

    protected override void ApplyEffect()
    {
        //Add speed increase to MovementComponent
        ScriptableFireSpeedBuff speedBuff = (ScriptableFireSpeedBuff)Buff;
        _playerComponent.shootCollDown += speedBuff.SpeedIncrease;
    }

    public override void End()
    {
        //Revert speed increase
        ScriptableFireSpeedBuff speedBuff = (ScriptableFireSpeedBuff)Buff;
        _playerComponent.shootCollDown -= speedBuff.SpeedIncrease * EffectStacks;
        EffectStacks = 0;
    }
}
