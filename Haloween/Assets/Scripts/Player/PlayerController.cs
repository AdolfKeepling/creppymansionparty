﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Animator))]
[RequireComponent(typeof(Rigidbody2D))]
public class PlayerController : MonoBehaviour
{
    private Rigidbody2D m_rigidbody2d;
    private Animator m_animator;
    protected PlatformerCharacter2D m_Character;
    protected PlayerInputController m_playerInput;

    [Header("Attack")]
    public GameObject bulletPrefab;
    public GameObject rightBulletSpawn;
    public GameObject upBulletSpawn;

    public float shootCollDown;

    public string attackSoundKey;
    [SerializeField]
    protected float m_coolDownTimer = 0;

    #region Properties 
    public bool IsAttackAvaliable { get; set; }

    protected float Horizontal { get; set; }
    protected float Vertical { get; set; }
    private bool Fire { get; set; }
    protected void HorizontalInput(float value)
    {
        Horizontal = value;
        m_Character.Move(Horizontal);

    }
    protected void VericalInput(float value)
    {
        Vertical = value;
    }
    protected void JumpInput()
    {
            m_Character.Jump();
    }

    private void FireInput()
    {
        if (IsAttackAvaliable)
        {
            Fire = true;            
        }
    }
    #endregion
    private void OnEnable()
    {

    }

    private void OnDisable()
    {       
        DirectUnSignInput();
    }

    void Awake()
    {
        m_rigidbody2d = GetComponent<Rigidbody2D>();
        m_animator = GetComponent<Animator>();
        m_Character = GetComponent<PlatformerCharacter2D>();
        m_playerInput = GetComponent<PlayerInputController>();

        DirectSignInput();

        IsAttackAvaliable = true;
    }
   
    // Update is called once per frame
    void Update()
    {
        CoolDown();
        AnimController();
    }
    #region SIGN INPUT

    public void DirectSignInput()
    {
        m_playerInput.horizontalInput += HorizontalInput;
        m_playerInput.verticalInput += VericalInput;
        m_playerInput.fireInput += FireInput;
        m_playerInput.jumpInput += JumpInput;
    }
    public void DirectUnSignInput()
    {
        m_playerInput.horizontalInput -= HorizontalInput;
        m_playerInput.verticalInput -= VericalInput;
        m_playerInput.fireInput -= FireInput;
        m_playerInput.jumpInput -= JumpInput;
    }

    public void InverseSignInput()
    {
        m_playerInput.horizontalInput += VericalInput;
        m_playerInput.verticalInput += HorizontalInput;
        m_playerInput.fireInput += JumpInput;
        m_playerInput.jumpInput += FireInput;
    }
    public void InverseUnSignInput()
    {
        m_playerInput.horizontalInput -= VericalInput;
        m_playerInput.verticalInput -= HorizontalInput;
        m_playerInput.fireInput -= JumpInput;
        m_playerInput.jumpInput -= FireInput;
    }

    #endregion SIGN INPUT

    #region Attack
    protected virtual void CoolDown()
    {
        if (!IsAttackAvaliable)
        {
            if (m_coolDownTimer >= shootCollDown)
            {
                IsAttackAvaliable = true;
            }
            else
            {
                m_coolDownTimer += Time.deltaTime;
            }
        }
    }

    public void SpawnHorizontalBullet()
    {
        SpawnBullet(rightBulletSpawn.transform);
    }

    public void SpawnVerticalBullet()
    {
        SpawnBullet(upBulletSpawn.transform);
    }

    private void SpawnBullet(Transform spawnT)
    {
        PlayerFireOver();

        if (m_Character.m_Wall) return;

        GameObject bullet = Instantiate(bulletPrefab, spawnT.position, spawnT.rotation);
        if (transform.localScale.x < 0)
            bullet.transform.localScale = new Vector3(-1* bullet.transform.localScale.x, bullet.transform.localScale.y, bullet.transform.localScale.z);
    }

    private void PlayerFireOver()
    {
        Fire = false;
        IsAttackAvaliable = false;
        m_coolDownTimer = 0;
    }
    #endregion

    // ПЕРЕПИСАТЬ ЕГО на прослушку свойств 
    #region Animation
    private void AnimController()
    {
        m_animator.SetBool("Horizontal", Horizontal != 0);
        m_animator.SetBool("Vertical", Vertical != 0);
        m_animator.SetBool("isGround", m_Character.m_Grounded);
        m_animator.SetBool("isFire", Fire);
    }
    #endregion
}