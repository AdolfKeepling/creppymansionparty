﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class TriggerActionByEvent : AbstractByEvent
{
    public string enterTag = "";
    public UnityEvent onTriggerEnter;
    [Space(20)]
    public string exitTag = "";
    public UnityEvent onTriggerExit;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (enterTag == collision.tag)
        {
            onTriggerEnter.Invoke();
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (exitTag == collision.tag)
        {
            onTriggerExit.Invoke();
        }
    }
}
