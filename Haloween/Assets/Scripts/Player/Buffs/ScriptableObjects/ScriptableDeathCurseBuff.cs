﻿using UnityEngine;

namespace ScriptableObjects
{
    [CreateAssetMenu(menuName = "Buffs/DeathCurseBuff")]
    public class ScriptableDeathCurseBuff : ScriptableBuff
    {
        public override TimedBuff InitializeBuff(GameObject obj)
        {
            return new TimedDeathCurseBuff(this, obj);
        }
    }
}
