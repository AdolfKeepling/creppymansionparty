﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections.Generic;

[ExecuteInEditMode]
[RequireComponent(typeof(Camera))]
public class SpritelightPostRffrct : MonoBehaviour
{
    private Material matP;
    private Camera camP;

    private Camera thisCamera;
    public LayerMask lightLayers;
    RenderTexture renderTextureP;


    internal void OnDisable()
    {
        //if (camP)
        //    DestroyImmediate(camP.gameObject);     
    }

    void Start()
    {
        
    }

   

    public void OnRenderImage(RenderTexture source, RenderTexture destination)
    {
        if (!Init())
            return;

        camP.CopyFrom(thisCamera);
        camP.backgroundColor = Color.black;
        camP.cullingMask =  lightLayers;

        renderTextureP = RenderTexture.GetTemporary(camP.pixelWidth, camP.pixelHeight, 0, RenderTextureFormat.DefaultHDR);

        camP.targetTexture = renderTextureP;
        camP.Render();
        
        matP.SetTexture("_LightTex", renderTextureP);

        Graphics.Blit(source, destination, matP);

        RenderTexture.ReleaseTemporary(renderTextureP);
    }

    private bool Init()
    {
        if (!thisCamera)
        {
            thisCamera = GetComponent<Camera>();

            if (!thisCamera)
                return false;
        }

        if (!matP)
        {
            Shader shader = Shader.Find("Hidden/LightPostEffect"); 
            if (!shader)
                return false;
            matP = new Material(shader);
        }

        if (!camP)
        {
            camP = new GameObject().AddComponent<Camera>();
            camP.enabled = false;
            camP.gameObject.hideFlags = HideFlags.HideAndDontSave;
        }

        return true;
    }

}
