﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StairsController : AbstractUsableItemsController
{
    public Transform spawnT;

    public string floorNumber;

    protected new void Start()
    {
        base.Start();
        distToPlayer = 0.3f;
    }

    protected override void ActionInput(GameObject player)
    {
        if (!isVisible) return;

        if (Vector2.Distance(player.transform.position, transform.position) < distToPlayer)
        {
            player.transform.position = spawnT.position;
            EventManager.SendEvent(EventKeys.SHOW_FLOOR_TEXT, floorNumber);
        }
    }
}
