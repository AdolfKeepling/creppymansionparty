﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Layers
{
    public const int GROUND = 256;
    public const int PLAYER_LAYER = 512;
    public const int ENEMY_LAYER = 1024;

    public const int GROUND_NUM = 8;
    public const int PLAYER_LAYER_NUM = 9;
    public const int ENEMY_LAYER_NUM = 10;
}
