﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VampireBossController : AICharcterController
{
    public GameObject celingGO;
    public GameObject batGO;

    public GameObject healthCanvas;

    private string m_animKeyHideBlood = "HideBlood";
    private string m_animKeyShowBlood = "ShowBlood";
    private string m_animKeyHideBat = "HideBat";
    private string m_animKeyShowBat = "ShowBat";

    private float m_commonAttackTimer = 5f;
    private float m_bloodAttackTimer = 10f;
    private float m_batAttackTimer = 8f;
    private enum AttackStates
    {
        common,
        blood,
        bat
    }
    private AttackStates m_attackStates;
    private AttackStates AttackState
    {
        get { return m_attackStates; }
        set
        {
            if (value != m_attackStates)
            {
                //print("[ATTACK state OUT] " + m_attackStates);
                AttackStateOUT();
                m_attackStates = value;
                //print("[ATTACK state IN] " + m_attackStates);
                AttackStateIN();
                
            }
        }
    }

    private bool IsBlood { get; set; }
    private bool IsBat { get; set; }
    private bool IsAttack { get; set; }

    protected new void Start()
    {
        m_Attack = GetComponent<CharacterAttackAbstract>();
        GetMovePoints();
        State = AIState.idle;

        healthCanvas.GetComponentInChildren<HealthBar>().InitHelathBar(gameObject);
        healthCanvas.SetActive(false);
    }


    protected new void OnBecameVisible()
    {
        m_isVisible = true;
    }

    protected new void OnBecameInvisible()
    {
        State = AIState.idle;
        m_isVisible = false;
    }

    protected override void GetMovePoints()
    {
        Debug.Log("Empty GetMovePoints");
    }

    private IEnumerator SwitchAttckState()
    {
        while (true)
        {
            AttackState = AttackStates.common;
            yield return new WaitForSeconds(m_commonAttackTimer);

            AttackState = AttackStates.blood;
            yield return new WaitForSeconds(m_bloodAttackTimer);

            AttackState = AttackStates.common;
            yield return new WaitForSeconds(m_commonAttackTimer);

            AttackState = AttackStates.bat;
            yield return new WaitForSeconds(m_batAttackTimer);
        }
    }

    public void HideSprite()
    {
        print("HideSprite");
        m_SpriteRenderer.color = new Color(0,0,0,0);
    }
    public void ShowSprite()
    {
        m_SpriteRenderer.color = new Color(1,1,1,1);
    }

    #region ATTACK STATES IN
    private void AttackStateIN()
    {
        switch (AttackState)
        {
            case AttackStates.common: { CommonAttackIN(); break; }
            case AttackStates.blood: { BloodAttackIN(); break; }
            case AttackStates.bat: { BatAttackIN(); break; }
        }
    }

    private void CommonAttackIN()
    {

    }

    private void BloodAttackIN()
    {
        StartCoroutine(BloodAttackLifeTime());
    }

    private IEnumerator BloodAttackLifeTime()
    {
        IsBlood = true;
        yield return new WaitForSeconds(1f);
        BlockBody(true);
        celingGO.SetActive(true);

        yield return new WaitForSeconds(m_bloodAttackTimer-1f);
        IsBlood = false;
        celingGO.SetActive(false);

        yield return new WaitForSeconds(0.5f);
        ShowSprite();
        BlockBody(false);
    }

    private void BatAttackIN()
    {
        StartCoroutine(BatAttackLifeTime());
    }

    private IEnumerator BatAttackLifeTime()
    {
        IsBat = true;
        yield return new WaitForSeconds(1f);
        BlockBody(true);
        batGO.transform.position = transform.position;
        batGO.SetActive(true);

        yield return new WaitForSeconds(m_batAttackTimer - 1f);
        IsBat = false;
        batGO.SetActive(false);

        yield return new WaitForSeconds(0.5f);
        ShowSprite();
        BlockBody(false);
    }

    private void BlockBody(bool _value)
    {
        if (_value)
        {
            m_RigBd.constraints = RigidbodyConstraints2D.FreezeAll;
            m_collider.enabled = false;
            m_Health.isDestroyable = false;
        }
        else
        {
            m_RigBd.constraints = RigidbodyConstraints2D.None;
            m_RigBd.constraints = RigidbodyConstraints2D.FreezeRotation;
            m_collider.enabled = true;
            m_Health.isDestroyable = true;
        }
    }
    #endregion ATTACK STATES IN

    #region ATTACK STATES
    private void CommonAttack()
    {   
        CheckDistanceToTarget();
        LookAtTarget(Target.transform);
        IsAttack = m_Attack.IsAttackAvaliable;
    }
    #endregion ATTACK STATES

    #region ATTACK STATES OUT
    private void AttackStateOUT()
    {
        switch (AttackState)
        {
            case AttackStates.common: { CommonAttackOUT(); break; }
            case AttackStates.blood: { BloodAttackOUT(); break; }
            case AttackStates.bat: { BatAttackOUT(); break; }
        }
    }

    private void CommonAttackOUT()
    {

    }

    private void BloodAttackOUT()
    {
        StopCoroutine(BloodAttackLifeTime());
    }

    private void BatAttackOUT()
    {
        m_Health.TakeHeal(1, gameObject);
        StopCoroutine(BatAttackLifeTime());
    }
    #endregion ATTACK STATES OUT

    protected override void AnimController()
    {
        m_Anim.SetBool("isAttack", IsAttack);
        m_Anim.SetBool("isBat", IsBat);
        m_Anim.SetBool("isBlood", IsBlood);
    }


    protected override void CheckDistanceToTarget()
    {
        if (Vector2.Distance(transform.position, Target.transform.position) < attackRadius)
        {
            State = AIState.attack;

        }
        else
            State = AIState.idle;
    }

    #region STATES
    protected override void Behaviour()
    {
        switch (State)
        {
            case AIState.idle: { Idle(); break; }
            case AIState.attack: { Attack(); break; }
        }
    }
    protected override void Attack()
    {
        if (Target != null)
        {
            switch (AttackState)
            {
                case AttackStates.common: { CommonAttack(); break; }
            }

        }
        else
            State = AIState.idle;
    }
    #endregion STATES


    #region ABSTRACT STATES IN
    protected override void BehaviourIN()
    {
        switch (State)
        {
            case AIState.idle: { IdleIN(); break; }        
            case AIState.attack: { AttackIN(); break; }
            case AIState.die: { DieIN(); break; }
        }
    }

    protected override void IdleIN()
    {
        IsMovement = false;
    }
    protected override void AttackIN()
    {
        healthCanvas.SetActive(true);
        IsMovement = false;
        AttackState = AttackStates.common;
        StartCoroutine(SwitchAttckState());
    }
    protected override void DieIN()
    {
        IsMovement = false;
    }
    #endregion ABSTRACT STATES IN


    #region ABSTRACT STATES OUT
    protected override void BehaviourOUT()
    {
        switch (State)
        {
            case AIState.idle: { IdleOUT(); break; }      
            case AIState.attack: { AttackOUT(); break; }
        }
    }

    protected override void IdleOUT()
    {
        IsMovement = false;
    }
    protected override void AttackOUT()
    {
        IsMovement = false;
        healthCanvas.SetActive(false);
    }
    #endregion ABSTRACT STATES OUT
}
