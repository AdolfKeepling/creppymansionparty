﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthAI : Health
{

    public string deathEvent;
    public override void TakeDamage(int damage, GameObject fromWho)
    {
        base.TakeDamage(damage, fromWho);
        StartCoroutine(TakeDamageAnimation());
    }

    public override void MyDestroy()
    {
        base.MyDestroy();
        EventManager.SendEvent(deathEvent);
        Destroy(gameObject);
    }

    public override void TakeHeal(int heal, GameObject fromWho)
    {
        healthPoint += (healthPoint + heal <= healthPointMax) ? heal : 0;
        base.TakeHeal(heal, fromWho);
    }

    private IEnumerator TakeDamageAnimation()
    {
        if (isDestroyable)
        {
            isDestroyable = false;
            for (int i = 0; i < 4; i++)
            {
                spriteRenderer.color = i % 2 == 0 ? new Color(1, 1, 1, 0) : new Color(1, 1, 1, 1); ;
                yield return new WaitForSeconds(0.15f);
            }
            isDestroyable = true;
        }
    }
}
