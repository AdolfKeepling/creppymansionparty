﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UniRx;

public class EventManager
{
    public MonoBehaviour sender { get; private set; } // MonoBehaviour отправителя
    public string key { get; private set; } // id сообщения
    public System.Object data { get; private set; } // данные


    public EventManager(MonoBehaviour sender, string key, System.Object data)
    {
        this.sender = sender;
        this.key = key;
        this.data = data;
    }

    public static EventManager Create(MonoBehaviour sender,
        string key, System.Object data)
    {
        return new EventManager(sender, key, data);
    }

    #region SENT EVENT
    public static void SendEvent(MonoBehaviour mono, string key, object data)
    {
        MessageBroker.Default
            .Publish(EventManager.Create(
            mono, // sender MonoBehaviour
            key, // message id
            data // data System.Ojbect
        ));
    }

    public static void SendEvent(MonoBehaviour mono, string key)
    {
        MessageBroker.Default
            .Publish(EventManager.Create(
            mono, // sender MonoBehaviour
            key, // message id
            null // data System.Ojbect
        ));
    }

    public static void SendEvent(string key, object data)
    {
        MessageBroker.Default
            .Publish(EventManager.Create(
            null, // sender MonoBehaviour
            key, // message id
            data // data System.Ojbect
        ));
    }

    public static void SendEvent(string key)
    {
        if (key == "")
        {
            return;
        }
        MessageBroker.Default
            .Publish(EventManager.Create(
            null, // sender MonoBehaviour
            key, // message id
            null // data System.Ojbect
        ));
    }
    #endregion
}
