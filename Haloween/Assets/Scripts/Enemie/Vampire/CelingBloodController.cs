﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CelingBloodController : MonoBehaviour
{
    [SerializeField]
    private Vector2 m_bloodRange;
    [SerializeField]
    private float m_step = 0.3f;
    [SerializeField]
    private float m_delay;
    [SerializeField]
    private GameObject m_bloodBulletPrefab;

    private void OnEnable()
    {
        StartCoroutine(BulletRain());
    }

    private void OnDisable()
    {
        StopCoroutine(BulletRain());
    }

    private IEnumerator BulletRain()
    {
        while (true)
        {
            if (Common.IsPause) yield return null;

            yield return new WaitForSeconds(m_delay);
            SpawnBullets(13, m_bloodRange.x + m_step, m_step);
            yield return new WaitForSeconds(m_delay);
            SpawnBullets(13, m_bloodRange.x - m_step * 0.5f, m_step);
        }
    }

    void SpawnBullets(int _count, float _startPos, float _step)
    {
        for (int i = 0; i < _count; i++)
        {
            Vector3 spawnPos = transform.position + new Vector3(_startPos + _step * i,0,0);
            Instantiate(m_bloodBulletPrefab, spawnPos, transform.rotation);
        }
    }
}
