﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuUIController : AbstractUIController
{
    SelectorController selector;

    private void Start()
    {
        selector = GetComponentInChildren<SelectorController>();
    }

    protected void Up()
    {
        selector.MoveUp();
    }
    protected void Down()
    {
        selector.MoveDown();
    }
    protected void Submit()
    {
        selector.Submit();
    }
    protected override void CreateDictionary()
    {
        inputHolder.Add(InputKeys.EXIT, Exit);
        inputHolder.Add(InputKeys.SUBMIT, Submit);
        inputHolder.Add(InputKeys.UP, Up);
        inputHolder.Add(InputKeys.DOWN, Down);
    }
    protected override void DestroyDictionary()
    {
        inputHolder.Clear();
    }

    #region Pause Functions
    public void Play()
    {
        EventManager.SendEvent(EventKeys.LOAD_LEVEL_NAME, Scenes.TUTORIAL);
    }
    public void Settings()
    {
        EventManager.SendEvent(EventKeys.UPDATE_UI_STATE, UIAppStates.settings);
    }
    public void Exit()
    {
        Debug.LogError("EXIT GAME");
        Application.Quit();
    }
    #endregion
}
