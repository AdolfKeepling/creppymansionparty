﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WitchBossController : AICharcterController
{
    private float m_redAttackTimer = 5f;
    private float m_greenAttackTimer = 5f;
    private float m_pinkAttackTimer = 5f;

    public GameObject redBullet;
    public GameObject greenBullet;
    public GameObject pinkBullet;

    private RangeAttack m_RangeAttack;

    [SerializeField] GameObject healthCanvas;

    private enum AttackStates
    {
        red,
        green,
        pink
    }

    private AttackStates m_attackStates;
    private AttackStates AttackState
    {
        get { return m_attackStates; }
        set
        {
            if (value != m_attackStates)
            {
                //print("[ATTACK state OUT] " + m_attackStates);
                AttackStateOUT();
                m_attackStates = value;
                //print("[ATTACK state IN] " + m_attackStates);
                AttackStateIN();

            }
        }
    }

    protected new void Start()
    {
        m_Attack = GetComponent<CharacterAttackAbstract>();
        m_RangeAttack = GetComponent<RangeAttack>();

        GetMovePoints();
        State = AIState.idle;

        healthCanvas.GetComponentInChildren<HealthBar>().InitHelathBar(gameObject);
        healthCanvas.SetActive(false);
    }

    protected new void OnBecameVisible()
    {
        m_isVisible = true;
    }

    protected new void OnBecameInvisible()
    {
       // State = AIState.idle;
        m_isVisible = false;
    }

    private IEnumerator SwitchAttckState()
    {
        while (true)
        {
            AttackState = AttackStates.red;
            yield return new WaitForSeconds(m_redAttackTimer);

            AttackState = AttackStates.green;
            yield return new WaitForSeconds(m_greenAttackTimer);

            AttackState = AttackStates.pink;
            yield return new WaitForSeconds(m_pinkAttackTimer);
        }
    }

    #region ATTACK STATES IN
    private void AttackStateIN()
    {
        switch (AttackState)
        {
            case AttackStates.red: { RedAttackIN(); break; }
            case AttackStates.green: { GreenAttackIN(); break; }
            case AttackStates.pink: { PinkAttackIN(); break; }
        }
    }

    private void RedAttackIN()
    {
        m_RangeAttack.bulletRangePrefab = redBullet;
    }

    private void GreenAttackIN()
    {
        m_RangeAttack.bulletRangePrefab = greenBullet;
    }

    private void PinkAttackIN()
    {
        m_RangeAttack.bulletRangePrefab = pinkBullet;
    }
    #endregion ATTACK STATES IN

    #region ATTACK STATES
    private void BottleAttack()
    {

    }
    #endregion ATTACK STATES

    #region ATTACK STATES OUT
    private void AttackStateOUT()
    {
        switch (AttackState)
        {
            case AttackStates.red: { RedAttackOUT(); break; }
            case AttackStates.green: { GreenAttackOUT(); break; }
            case AttackStates.pink: { PinkAttackOUT(); break; }
        }
    }

    private void RedAttackOUT()
    {

    }

    private void GreenAttackOUT()
    {

    }

    private void PinkAttackOUT()
    {
 
    }
    #endregion ATTACK STATES OUT

    protected override void CheckDistanceToTarget()
    {
        if (Vector2.Distance(transform.position, Target.transform.position) < attackRadius)
        {
            State = AIState.attack;

        }
        else
            State = AIState.idle;
    }

    protected override void FollowObject(GameObject _obj)
    {
        if (IsMovement)
        {
            if (_obj != null)
            {
                transform.position = Vector3.MoveTowards(transform.position, _obj.transform.position, speed * Time.deltaTime);
                //transform.position += Vector3.right * speed * Time.deltaTime;
            }
        }
    }

    protected override void WalkOnMovePoints()
    {
        if (Vector3.Distance(transform.position, m_movePoints[m_mpIndex].position) < m_distToPoint)
        {
            m_mpIndex = Common.UpdateSelectedIndexInList(m_mpIndex, m_movePoints.Count, true);
            LookAtTarget(m_movePoints[m_mpIndex]);
        }
        FollowObject(m_movePoints[m_mpIndex].gameObject);
    }

    protected override void Flip()
    {
        // Multiply the player's x local scale by -1.
        Vector3 theScale = transform.localScale;
        theScale.x *= -1;
        transform.localScale = theScale;
    }

    #region STATES
    protected override void Behaviour()
    {
        switch (State)
        {
            case AIState.idle: { Idle(); break; }
            case AIState.attack: { Attack(); break; }
        }
    }

    protected override void Idle()
    {
        base.Idle();
        WalkOnMovePoints();
    }

    protected override void Attack()
    {
        WalkOnMovePoints();

        if (Target != null)
        {
            BottleAttack();

        }
        else
            State = AIState.idle;
    }
    #endregion STATES


    #region ABSTRACT STATES IN
    protected override void BehaviourIN()
    {
        switch (State)
        {
            case AIState.idle: { IdleIN(); break; }
            case AIState.attack: { AttackIN(); break; }
            case AIState.die: { DieIN(); break; }
        }
    }

    protected override void IdleIN()
    {
        IsMovement = false;
    }
    protected override void AttackIN()
    {
        IsMovement = true;
        healthCanvas.SetActive(true);
        StartCoroutine(SwitchAttckState());
    }
    protected override void DieIN()
    {
        IsMovement = false;
    }
    #endregion ABSTRACT STATES IN


    #region ABSTRACT STATES OUT
    protected override void BehaviourOUT()
    {
        switch (State)
        {
            case AIState.idle: { IdleOUT(); break; }
            case AIState.attack: { AttackOUT(); break; }
        }
    }

    protected override void IdleOUT()
    {
        IsMovement = false;
    }
    protected override void AttackOUT()
    {
        IsMovement = false;
        healthCanvas.SetActive(true);
    }
    #endregion ABSTRACT STATES OUT
}
