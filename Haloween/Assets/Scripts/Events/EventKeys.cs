﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventKeys {


    #region SAVE
    /// <summary>
    /// reading drom save was rught now 
    /// </summary>
    public static string RESTORE_FROM_STORAGE = "RESTORE_FROM_STORAGE";

    /// <summary>
    /// load from starage, menu or pause 
    /// </summary>
    public static string LOAD_FROM_STORAGE = "LOAD_FROM_STORAGE";

    /// <summary>
    /// save data to storage, CALLS FROM UI!!!
    /// </summary>
    public static string SAVE_DATA_TO_STORAGE = "SAVE_DATA_TO_STORAGE";

    public static string EXCEPTION__LOAD_SLOT_IS_EMPTY = "EXCEPTION__LOAD_SLOT_IS_EMPTY";

    public static string SAVE_SETTINGS = "SAVE_SETTINGS";

    #endregion

    #region Game Events
    /// <summary>
    /// activate new achievement 
    /// </summary>
    public static string ACTIVATE_ACHIEVEMENT = "ACTIVATE_ACHIEVEMENT";

    /// <summary>
    /// event to camera , shake camera
    /// </summary>
    public static string CAMERA_SHAKE = "CAMERA_SHAKE";
    #region Load Level
    /// <summary>
    /// event to scene helper , load level "level name"
    /// </summary>
    public static string LOAD_LEVEL_NAME = "LOAD_LEVEL_NAME";
    /// <summary>
    /// event to scene helper , load level "level number"
    /// </summary>
    public static string LOAD_LEVEL_NUM = "LOAD_LEVEL_NUM";
    /// <summary>
    /// event to scene helper , load next level in game build
    /// </summary>
    public static string LOAD_NEXT_LEVEL = "LOAD_NEXT_LEVEL";
    /// <summary>
    /// event to scene helper , load next level in game build with dellay 1.5f sec
    /// </summary>
    public static string LOAD_NEXT_LEVEL_DELAY = "LOAD_NEXT_LEVEL_DELAY";
    /// <summary>
    /// event to scene helper , reload current level
    /// </summary>
    /// <summary>
    /// event to scene helper , load previous level in game build
    /// </summary>
    public static string LOAD_PREV_LEVEL = "LOAD_PREV_LEVEL";
    /// <summary>
    /// event to scene helper , reload current level in game build
    /// </summary>
    public static string RELOAD_LEVEL = "RELOAD_LEVEL";
    #endregion
    /// <summary>
    /// show next level door
    /// </summary>
    public static string SHOW_NEXT_LEVEL_DOOR = "SHOW_NEXT_LEVEL_DOOR";

    /// <summary>
    /// some gameplay important event was done
    /// </summary>
    public static string LOCALIZATE_TEXT = "LOCALIZATE_TEXT";

    /// <summary>
    /// Game over
    /// </summary>
    public static string GAME_OVER = "GAME_OVER";
    #endregion

    #region Player Events 
    /// <summary>
    /// event to PLAYER, AvaliableMovement = FALSE
    /// </summary>
    public static string STOP_PLAYER_MOVEMENT = "STOP_PLAYER_MOVEMENT";
    /// <summary>
    /// event to PLAYER, AvaliableMovement = TRUE
    /// </summary>
    public static string START_PLAYER_MOVEMENT = "START_PLAYER_MOVEMENT";

    /// <summary>
    /// event to PLAYER, Sprite renderer.enable = FALSE
    /// </summary>
    public static string HIDE_PLAYER = "HIDE_PLAYER";

    /// <summary>
    /// event to PLAYER, Sprite renderer.enable = TRUE
    /// </summary>
    public static string SHOW_PLAYER = "HIDE_PLAYER";

    /// <summary>
    /// do inverce debuff
    /// </summary>
    public static string DO_INVERSE_DEBUFF = "DO_INVERSE_DEBUFF";

    /// <summary>
    /// player died
    /// </summary>
    public static string PLAYER_DIED = "PLAYER_DIED";

    /// <summary>
    /// player press action button
    /// </summary>
    public static string PLAYER_DO_ACTION = "PLAYER_DO_ACTION";
    #endregion

    #region  INPUT
    /// <summary>
    /// USER MAKE INPUT FROM GLOBAL INPUT HELPER 
    /// </summary>
    public static string PLAYER_PRESS_BUTTON = "PLAYER_PRESS_BUTTON";
    #endregion

    #region UI
    public static string UPDATE_UI_STATE = "UPDATE_UI_STATE";

    public static string UPDATE_CURRENT_LANGUAGE = "UPDATE_CURRENT_LANGUAGE";

    public static string GO_TO_MENU = "GO_TO_MENU";

    public static string SHOW_FLOOR_TEXT = "SHOW_FLOOR_TEXT";
    public static string SHOW_FLOOR_TEXT_START = "SHOW_FLOOR_TEXT_START";
    #endregion UI


    public static string ACTIVATE_DE_BUFF = "ACTIVATE_DE_BUFF";
    public static string ACTIVATE_DEATH_DE_BUFF = "ACTIVATE_DEATH_DE_BUFF";
    public static string ACTIVATE_BUFF = "ACTIVATE_BUFF";

    public static string DE_ACTIVATE_BUFF = "DE_ACTIVATE_BUFF";
    public static string DE_ACTIVATE_DE_BUFF = "DE_ACTIVATE_DE_BUFF";
    public static string DE_ACTIVATE_DEATH_DE_BUFF = "DE_ACTIVATE_DEATH_DE_BUFF";



}
