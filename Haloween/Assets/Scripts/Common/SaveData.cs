﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;

[System.Serializable]
public class SaveData
{

    public string saveName;

    public int sceneIndex;

    //saved data
    public string dateTime;

    //public index was done
    public List<GameEvent> gameEvents;

    [JsonConstructor]
    public SaveData(string saveName, int sceneIndex, string dateTime, List<GameEvent> gameEvents)
    {
        this.saveName = saveName;
        this.sceneIndex = sceneIndex;
        this.dateTime = dateTime;

        this.gameEvents = gameEvents;
    }

    public SaveData(SaveData sd)
    {
        this.saveName = sd.saveName;
        this.sceneIndex = sd.sceneIndex;
        this.dateTime = sd.dateTime;   
        this.gameEvents = sd.gameEvents;
    }

    public SaveData(string saveName, int sceneIndex, string dateTime)
    {
        this.saveName = saveName;
        this.sceneIndex = sceneIndex;
        this.dateTime = dateTime;
    }
}