﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletController : AbstractBulletController
{

    protected new void Start()
    {
        base.Start();
        speed = transform.localScale.x < 0 ? -speed : speed;
    }

    protected override void Movement()
    {
        transform.position += transform.right * speed * Time.deltaTime;
    }
}
