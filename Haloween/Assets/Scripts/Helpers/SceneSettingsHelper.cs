﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneSettingsHelper : MonoBehaviour
{
    void Awake()
    {
        IgnoreLayers();
    }

    protected void IgnoreLayers()
    {
        Physics2D.IgnoreLayerCollision(Layers.ENEMY_LAYER_NUM, Layers.ENEMY_LAYER_NUM);
        Physics2D.IgnoreLayerCollision(Layers.PLAYER_LAYER_NUM, Layers.PLAYER_LAYER_NUM);
        Physics2D.IgnoreLayerCollision(Layers.GROUND_NUM, Layers.GROUND_NUM);
    }
}
