﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HidenItemController : AbstractUsableItemsController
{
    public Sprite openedSprite;
    public List<RandSpawnObj> randSpawnObj;
    private Transform spawnT;
    private Animator m_animator;

    public override bool IsOpened
    {
        get => isOpened;
        set
        {
            isOpened = value;
        }
    }

    protected new void Start()
    {
        base.Start();
        spawnT = transform.GetChild(0);
        m_animator = GetComponent<Animator>();
        distToPlayer = 0.45f;
        IsOpened = false;       
    }

    protected void SetDoorState()
    {
        if (m_animator!= null)
            m_animator.enabled = false;
        m_spriteRenderer.sprite = openedSprite;
    }

    protected override void ActionInput(GameObject player)
    {

        if (IsOpened || !isVisible) return;
        print("HidenItemController ActionInput");
        if (Vector2.Distance(player.transform.position, transform.position) < distToPlayer)
        {
            IsOpened = true;
            SetDoorState();
            SpawnObj();
        }
    }

    private void SpawnObj()
    {
        if (randSpawnObj.Count == 0) return;

        GameObject prefab = Common.GetRandowmObj(randSpawnObj);
        print(prefab.name);
        if (prefab != null)
            Instantiate(prefab, spawnT.position,spawnT.rotation);
    }
}
