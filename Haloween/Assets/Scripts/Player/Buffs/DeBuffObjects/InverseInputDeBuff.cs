﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class InverseInputDeBuff : AbstractDeBuff
{
   
    public override void DoDebuff(GameObject target)
    {
        print("DoDebuff=>" + target.name);
        TimedBuff inverseInput = new TimedInverseInputBuff(scriptableBuff, target);
        target.GetComponent<BuffableEntity>().AddBuff(inverseInput);
    }

}
