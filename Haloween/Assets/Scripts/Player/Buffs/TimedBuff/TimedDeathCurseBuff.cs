﻿using System.Collections;
using System.Collections.Generic;
using ScriptableObjects;
using UnityEngine;

public class TimedDeathCurseBuff : TimedBuff
{
    private readonly Health _playerHealth;

    public TimedDeathCurseBuff(ScriptableBuff buff, GameObject obj) : base(buff, obj)
    {
        //Getting MovementComponent, replace with your own implementation
        _playerHealth = obj.GetComponent<Health>();
        
    }

    protected override void ApplyEffect()
    {
        // SHOW IT ON UI
    }

    public override void End()
    {
        _playerHealth.TakeDamage();
    }
}