using UnityEngine;

namespace ScriptableObjects
{
    [CreateAssetMenu(menuName = "Buffs/MoveSpeedBuff")]
    public class ScriptableMoveSpeedBuff : ScriptableBuff
    {
        public float SpeedIncrease;

        public override TimedBuff InitializeBuff(GameObject obj)
        {
            return new TimedMoveSpeedBuff(this, obj);
        }
    }
}
