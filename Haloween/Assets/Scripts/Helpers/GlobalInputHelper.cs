﻿using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GlobalInputHelper : MonoBehaviour
{

    string[] Inputs = new string[] {
        InputKeys.RESTART,
        InputKeys.EXIT,
        InputKeys.SUBMIT,
        InputKeys.ACTION,
        InputKeys.UP,
        InputKeys.DOWN,
        InputKeys.LEFT,
        InputKeys.RIGHT
    };
 
    private void Update()
    {
        foreach (string x in Inputs)
            GetButton(x);
    }

    private void GetButton(string button)
    {         
        if (Input.GetButtonDown(button))
        {
            EventManager.SendEvent(EventKeys.PLAYER_PRESS_BUTTON, button);
        }
    }
}
