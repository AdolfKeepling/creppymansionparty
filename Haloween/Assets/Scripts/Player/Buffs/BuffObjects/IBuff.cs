﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IBuff
{
    float DestroyTime { get; set; }

    void SelfDestroy();

}
