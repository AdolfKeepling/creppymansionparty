﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThrowBulletController : AbstractBulletController
{
    protected new void Start()
    {
        base.Start();
        speed = transform.localScale.x < 0 ? -speed : speed;
        m_RigBd.AddForce(Vector2.right * speed);
    }

    protected override void Movement()
    {       
    }

    protected override void DetectCollision(GameObject coll)
    {   
        Health collHealth = coll.GetComponent<Health>();
        if (collHealth == null) return;

        if (collHealth.tag == m_Health.enemyTag)
        {
            switch (bulletAttackType)
            {
                case MeleeAttackType.splash: { SplashAttack(); break; }
                case MeleeAttackType.target: { TargetAttack(collHealth); break; }
                case MeleeAttackType.mixed: { TargetAttack(collHealth); SplashAttack(); break; }
            }

            m_Health.TakeDamage();
        }
    }
}
