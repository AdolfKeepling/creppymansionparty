﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HelthBullet : Health {


	protected override void Start ()
    {
        SelfDestroy();
    }

    public override void MyDestroy()
    {
        base.MyDestroy();
        Destroy(gameObject);
    }

    protected override void SpawnDestroyParticles()
    {
        if (destroyedUnit != null)
           Destroy(Instantiate(destroyedUnit, transform.position, transform.rotation),0.75f);
    }

    protected override void OnCollisionEnter2D(Collision2D coll)
    {
        //if (coll.gameObject.tag == Tags.Ground || coll.gameObject.tag == Tags.Wall || coll.gameObject.tag == Tags.Door)
        //{
        //    TakeDamage();
        //    return;
        //}
    }
}
