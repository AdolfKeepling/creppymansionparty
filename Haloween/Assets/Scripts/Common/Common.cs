﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public static class Common
{

    private static UIAppStates stateUI;

    public static string SAVE_SCENE_KEY = "SAVE_SCENE_KEY";
    public static string SAVE_SETTINGS_KEY = "SAVE_SETTINGS_KEY";

    public static int saveSlots = 4;
    public static List<string> saveStringList = new List<string> { "[empty]", "[empty]", "[empty]", "[empty]" };
    public static SaveData saveData;

    public static string currentLevelName;
    public static int currentLevelIndex = 0;

    private static bool isRestoreFromStorage = false;
    public static bool IsRestoreFromStorage
    {
        get
        {
            return isRestoreFromStorage;
        }
        set
        {
            isRestoreFromStorage = value;
        }
    }

    public static bool IsPause { get; set; }

    public static UIAppStates StateUI
    {
        get => stateUI;
        set
        {
            stateUI = value;
            IsPause = stateUI != UIAppStates.game;
        }
    }

    /// <summary>
    /// Loop list items change
    /// </summary>
    /// <param name="index"></param>
    /// <param name="listCount"></param>
    /// <param name="isNext"></param>
    public static int UpdateSelectedIndexInList(int index, int listCount, bool isNext)
    {
        if (isNext)
            index = index + 1 < listCount ? index + 1 : 0;
        else
            index = index - 1 >= 0 ? index - 1 : listCount - 1;

        return index;
    }

    /// <summary>
    /// Check is is last item of list
    /// </summary>
    /// <param name="index"></param>
    /// <param name="listCount"></param>
    /// <param name="isNext"></param>
    public static bool CheckLastListItem(int index, int listCount)
    {
        return index + 1 < listCount;
    }

    public static GameObject GetRandowmObj(List<RandSpawnObj> list)
    { 
        int rnd = Random.Range(0,100);
        GameObject obj = list.First(x => x.chance.x <= rnd && rnd < x.chance.y).prefab;
        return obj;
    }
}

public enum ItemColor
{
    blue = 0, red = 1, yellow = 2, pink = 3, green = 4
}

public static class PlayerAnimKey
{
    public const string Idle = "Idle";
    public const string Walk = "Walk";
    public const string Jump = "Jump";

    public const string IdleFire = "IdleFire";
    public const string WalkFire = "WalkFire";
    public const string JumpFire = "JumpFire";
    public const string UpFire = "UpFire";

    public const string Attack = "Attack";
}

[System.Serializable]
public class DoorType
{
    public ItemColor color;
    public Sprite closerDoor;
    public Sprite openedDoor;

    public DoorType(ItemColor color, Sprite closerDoor, Sprite openedDoor)
    {
        this.color = color;
        this.closerDoor = closerDoor;
        this.openedDoor = openedDoor;
    }
}

[System.Serializable]
public class RandSpawnObj
{
    public GameObject prefab;
    public Vector2 chance;

    public RandSpawnObj(GameObject prefab, Vector2 chance)
    {
        this.prefab = prefab;
        this.chance = chance;
    }
}

[System.Serializable]
public class GameEvent
{
    public string eventKey;
    public bool isComplated;
}
[System.Serializable]
public class StartEvents
{
    public string eventKey;
    public float eventDelay;
}


[System.Serializable]
public class Players
{
    public string playerName;
    public GameObject playerPrefab;
    public GameObject playerUIPrefab;
}
