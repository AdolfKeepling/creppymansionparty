﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "PlayerHolder", menuName = "ScriptableObjects/CreatePlayersHolder", order = 3)]
public class PlayersHolder : ScriptableObject
{
    public List<Players> players = new List<Players>();
}
