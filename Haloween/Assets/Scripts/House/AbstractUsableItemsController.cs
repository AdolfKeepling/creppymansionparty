﻿using System.Collections;
using System.Collections.Generic;
using UniRx;
using UnityEngine;


[RequireComponent(typeof(SpriteRenderer))]
public abstract class AbstractUsableItemsController : MonoBehaviour
{

    protected SpriteRenderer m_spriteRenderer;

    protected float distToPlayer = 0.4f;

    protected bool isOpened;
    protected bool isVisible;
    public virtual bool IsOpened
    {
        get;set;
    }

    protected void Start()
    {
        m_spriteRenderer = GetComponent<SpriteRenderer>();
    }

    #region EVENT
    protected CompositeDisposable _event;
    protected void OnEnable()
    {
        _event = new CompositeDisposable();
        MessageBroker.Default
            .Receive<EventManager>()
            .Where(msg => msg.key == EventKeys.PLAYER_DO_ACTION)
            .Subscribe(msg => {
                ActionInput((GameObject)msg.data);
            }).AddTo(_event);
    }

    protected void OnDisable()
    {
        if (_event != null)
        {
            _event.Dispose();
        }
    }
    #endregion

    protected void OnBecameVisible()
    {
        isVisible = true;
    }
    protected void OnBecameInvisible()
    {
        isVisible = false;
    }

    protected abstract void ActionInput(GameObject player);

}
