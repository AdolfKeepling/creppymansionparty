﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class SelectorSlider : SelectorButton
{
    [SerializeField]Text valueTxt;

    [SerializeField]Image arrowRightImg;
    [SerializeField]Image arrowLeftImg;

    public Text ValueTxt { get => valueTxt; set => valueTxt = value; }
    public Image ArrowRightImg { get => arrowRightImg; set => arrowRightImg = value; }
    public Image ArrowLeftImg { get => arrowLeftImg; set => arrowLeftImg = value; }
}
