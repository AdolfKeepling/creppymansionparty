﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[System.Serializable]
[RequireComponent(typeof(Health))]
[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(Animator))]
[RequireComponent(typeof(Collider2D))]
[RequireComponent(typeof(SpriteRenderer))]
public abstract class AbstractCharacterController : MonoBehaviour
{
    protected Rigidbody2D m_RigBd;
    protected Animator m_Anim;
    protected Health m_Health;
    protected SpriteRenderer m_SpriteRenderer;
    protected Collider2D m_collider;

    public virtual GameObject Target
    {
        get;set;
    }

    public enum AIState
    {
        idle = 0,
        patrol = 1,
        chase = 2,
        attack = 3,
        die = 4
    }
    [SerializeField]
    private AIState state = AIState.idle;
    public AIState State
    {
        get
        {
            return state;
        }
        set
        {
            if (state != value)
            {
                //print("BEHAV state " + value);
                BehaviourOUT();
                state = value;
                BehaviourIN();
            }
        }
    }
   
    private bool _isMoevement = false;
    public virtual bool IsMovement
    {
        get { return _isMoevement; }
        set
        {
            _isMoevement = value;
        }
    }

    protected void OnEnable()
    {
        m_Health.characterDie += DieCharacter;
    }

    protected void OnDisable()
    {
        m_Health.characterDie -= DieCharacter;
    }

    protected void Awake()
    {
        m_collider = GetComponent<Collider2D>();
        m_RigBd = GetComponent<Rigidbody2D>();
        m_Anim = GetComponent<Animator>();
        m_Health = GetComponent<Health>();
        m_SpriteRenderer = GetComponent<SpriteRenderer>();
    }

    protected void Update()
    {
        if (Common.IsPause) return;

        Behaviour();
    }

    protected virtual void OnCollisionEnter2D(Collision2D _collision)
    {     
    }
    protected virtual void OnTriggerEnter2D(Collider2D _collider)
    {
    }

    protected void CollisionDamage(GameObject _enemie,int _damage)
    {
        Health h = _enemie.GetComponent<Health>();
        if (h != null)
        {
            if (_enemie.tag == m_Health.enemyTag)
            {
                h.TakeDamage(_damage, gameObject);
            }
        }
    }

    public void DieCharacter()
    {
        
    }

    #region ABSTRACT STATES IN
    protected virtual void BehaviourIN()
    {
        switch (State)
        {
            case AIState.idle: { IdleIN(); break; }
            case AIState.patrol:
                {
                    PatrolIN();
                    break;
                }
            case AIState.attack: { AttackIN(); break; }
            case AIState.chase: { ChaseIN(); break; }
            case AIState.die: { DieIN(); break; }
        }
    }
    /// <summary>
    /// Idle state
    /// </summary>
    protected abstract void IdleIN();
    /// <summary>
    /// Patrol state
    /// </summary>
    protected abstract void PatrolIN();
    /// <summary>
    /// Attack state
    /// </summary>
    protected abstract void AttackIN();
    /// <summary>
    /// Chase state
    /// </summary>
    protected abstract void ChaseIN();
    /// <summary>
    /// Die state
    /// </summary>
    protected abstract void DieIN();
    #endregion ABSTRACT STATES IN

    #region ABSTRACT STATES
    protected virtual void Behaviour()
    {
        switch (State)
        {
            case AIState.idle: { Idle(); break; }
            case AIState.patrol:
                {
                    Patrol();
                    break;
                }
            case AIState.attack: { Attack(); break; }
            case AIState.chase: { Chase(); break; }
        }
    }
    /// <summary>
    /// Idle state
    /// </summary>
    protected abstract void Idle();
    /// <summary>
    /// Patrol state
    /// </summary>
    protected abstract void Patrol();
    /// <summary>
    /// Attack state
    /// </summary>
    protected abstract void Attack();
    /// <summary>
    /// Chase state
    /// </summary>
    protected abstract void Chase();
    #endregion ABSTRACT STATES

    #region ABSTRACT STATES OUT
    protected virtual void BehaviourOUT()
    {
        switch (State)
        {
            case AIState.idle: { IdleOUT(); break; }
            case AIState.patrol:
                {
                    PatrolOUT();
                    break;
                }
            case AIState.attack: { AttackOUT(); break; }
            case AIState.chase: { ChaseOUT(); break; }
        }
    }
    /// <summary>
    /// Idle state
    /// </summary>
    protected abstract void IdleOUT();
    /// <summary>
    /// Patrol state
    /// </summary>
    protected abstract void PatrolOUT();
    /// <summary>
    /// Attack state
    /// </summary>
    protected abstract void AttackOUT();
    /// <summary>
    /// Chase state
    /// </summary>
    protected abstract void ChaseOUT();
    #endregion ABSTRACT STATES OUT
}