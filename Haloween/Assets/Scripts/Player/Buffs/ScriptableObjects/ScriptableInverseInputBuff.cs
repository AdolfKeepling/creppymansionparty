using UnityEngine;

namespace ScriptableObjects
{
    [CreateAssetMenu(menuName = "Buffs/InverseInputBuff")]
    public class ScriptableInverseInputBuff : ScriptableBuff
    {
        public override TimedBuff InitializeBuff(GameObject obj)
        {
            return new TimedInverseInputBuff(this, obj);
        }
    }
}
