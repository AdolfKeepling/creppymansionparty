﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveSpeedBuff : AbstractUsableItemsController, IBuff
{
    // PINK SCRIPT
    [SerializeField] ScriptableBuff scriptableBuff;
    public float DestroyTime { get; set; } = 10f;

    protected new void Start()
    {
        base.Start();
        SelfDestroy();
    }

    public void SelfDestroy()
    {
        Destroy(gameObject, DestroyTime);
    }

    protected override void ActionInput(GameObject player)
    {
        if (!isVisible) return;

        if (Vector2.Distance(player.transform.position, transform.position) < distToPlayer)
        {
            TimedBuff speedBuff = new TimedMoveSpeedBuff(scriptableBuff, player);
            player.GetComponent<BuffableEntity>().AddBuff(speedBuff);
            Destroy(gameObject);
        }
    }
}