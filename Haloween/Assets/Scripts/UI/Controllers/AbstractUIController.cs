﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using System;
using UniRx;


public abstract class AbstractUIController : MonoBehaviour
{

    protected Dictionary<string, Action> inputHolder = new Dictionary<string, Action>();
    
        //InputKeys.RESTART,
        //InputKeys.EXIT,
        //InputKeys.SUBMIT,
        //InputKeys.ACTION,
        //InputKeys.UP,
        //InputKeys.DOWN,
        //InputKeys.LEFT,
        //InputKeys.RIGHT

    private CompositeDisposable disposables;
    private void OnEnable()
    {

        print(gameObject.transform.parent.name + " Enable");

        disposables = new CompositeDisposable();
        MessageBroker.Default
            .Receive<EventManager>() 
            .Where(msg => msg.key == EventKeys.PLAYER_PRESS_BUTTON)
            .Subscribe(msg => {
                CheckPressedButton((string)msg.data);
            }).AddTo(disposables);

        CreateDictionary();
    }

    private void OnDisable()
    {
        if (disposables != null)
        {
            disposables.Dispose();
        }
        DestroyDictionary();
    }

    protected abstract void CreateDictionary();
    protected abstract void DestroyDictionary();

    private void CheckPressedButton(string key)
    {
        Action action;
        if (inputHolder.TryGetValue(key, out action))
            action?.Invoke();
    }
}
