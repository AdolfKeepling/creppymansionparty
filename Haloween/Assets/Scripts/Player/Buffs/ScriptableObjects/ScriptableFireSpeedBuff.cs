using UnityEngine;

namespace ScriptableObjects
{
    [CreateAssetMenu(menuName = "Buffs/FireSpeedBuff")]
    public class ScriptableFireSpeedBuff : ScriptableMoveSpeedBuff
    {
        public override TimedBuff InitializeBuff(GameObject obj)
        {
            return new TimedFireSpeedBuff(this, obj);
        }
    }
}
