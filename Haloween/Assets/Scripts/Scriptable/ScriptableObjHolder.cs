﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class ScriptableObjHolder : MonoBehaviour
{

    public static ScriptableObjHolder instance;

    public DoorHolder doorHolder;

    void Awake()
    {
        instance = this;
    }


    public DoorType GetDoorType(ItemColor color)
    {
        return doorHolder.doorTypes.First(x=> x.color == color);
    }
}
