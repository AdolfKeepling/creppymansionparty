﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName = "PlayerInputs", menuName = "ScriptableObjects/CreatePlayerInputs", order = 2)]
public class PlayerInputsHolder : ScriptableObject
{
    public List<PlayerInput> playerInputs = new List<PlayerInput>();
}
