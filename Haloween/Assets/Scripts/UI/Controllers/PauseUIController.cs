﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseUIController : AbstractUIController
{
    SelectorController selector;

    private void Start()
    {
        selector = GetComponentInChildren<SelectorController>();
    }

    protected void Up()
    {
        selector.MoveUp();
    }
    protected void Down()
    {
        selector.MoveDown();
    }
    protected void Submit()
    {
        selector.Submit();
    }
    protected override void CreateDictionary()
    {
        inputHolder.Add(InputKeys.EXIT, Resume);
        inputHolder.Add(InputKeys.SUBMIT, Submit);
        inputHolder.Add(InputKeys.UP, Up);
        inputHolder.Add(InputKeys.DOWN, Down);
    }
    protected override void DestroyDictionary()
    {
        inputHolder.Clear();
    }

    #region Pause Functions

    public void Resume()
    {
        EventManager.SendEvent(EventKeys.UPDATE_UI_STATE, UIAppStates.game);
    }
    public void Menu()
    {
        EventManager.SendEvent(EventKeys.LOAD_LEVEL_NAME, Scenes.MENU);
    }
    public void Settings()
    {
        EventManager.SendEvent(EventKeys.UPDATE_UI_STATE, UIAppStates.settings);
    }    
    public void Exit()
    {
        Debug.LogError("EXIT GAME");
    }
    #endregion
}
