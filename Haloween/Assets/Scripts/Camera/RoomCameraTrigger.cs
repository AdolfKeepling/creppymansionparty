﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoomCameraTrigger : MonoBehaviour
{

    public List<Transform> cameraMovePoints;
    public List<AICharcterController> enemiesList = new List<AICharcterController>();

    [SerializeField] Transform m_player;
    [SerializeField] Transform m_camera;

    void Start()
    {
        GetCamera();
    }

    
    void Update()
    {
        MoveCamera();
    }

    private void GetCamera()
    {
        m_camera = FindObjectOfType<Camera>().transform;
    }

    private void SetCameraPos(Vector3 _newPos)
    {
        m_camera.position = _newPos + new Vector3(0,0,-10);
    }

    private void MoveCamera()
    {
        if (m_player == null) return;
        if (cameraMovePoints.Count <= 1) return;

        float x = Mathf.Clamp(m_player.position.x, cameraMovePoints[0].position.x, cameraMovePoints[1].position.x);
        SetCameraPos(new Vector3(x, m_camera.position.y,0));
    }

    private void SetCameraPosition()
    {
        if (cameraMovePoints.Count == 1)
        {
            SetCameraPos(cameraMovePoints[0].position);
        }
        else if(cameraMovePoints.Count > 1)
        {
            float dist0 = Vector2.Distance(cameraMovePoints[0].position, m_player.position);
            float dist1 = Vector2.Distance(cameraMovePoints[1].position, m_player.position);

            SetCameraPos(cameraMovePoints[dist0 < dist1? 0 : 1].position);
        }
    }

    private void OnTriggerEnter2D(Collider2D _collision)
    {
        if (_collision.tag == Tags.Player)
        {
            if (_collision.GetComponent<PlayerController>() != null)
            {
                m_player = _collision.transform;
                SetCameraPosition();
            }
        }
        //if (collision.tag == Tags.ENEMY)
        //{
        //    AICharcterController ai = collision.GetComponent<AICharcterController>();
        //    if ( ai != null)
        //    {
        //        enemiesList.Add(ai);
        //    }
        //}
    }
    private void OnTriggerExit2D(Collider2D _collision)
    {
        if (_collision.tag == Tags.Player)
        {
            if (_collision.GetComponent<PlayerController>() != null)
            {
                m_player = null;
            }
        }
    }
}
