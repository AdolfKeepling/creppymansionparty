﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public enum MeleeAttackType { target, splash, mixed }

[RequireComponent(typeof(Health))]
[RequireComponent(typeof(AbstractCharacterController))]
public abstract class CharacterAttackAbstract : MonoBehaviour
{
    public float shootCollDown; 
    public int damage;

    public string attackSoundKey;

    public float raycastSplashRadius;
    public MeleeAttackType splashAttackType;

    protected Health m_Health;
    protected AbstractCharacterController m_Char;

    public delegate void AttackDelegate();
    public AttackDelegate shotDelegate;
    public AttackDelegate slashDelegate;
    public AttackDelegate targetDelegate;

    [SerializeField]
    protected float m_coolDownTimer = 0;
    public bool IsAttackAvaliable { get; set; } = false;

    protected void Start()
    {
        m_Health = GetComponent<Health>();
        m_Char = GetComponent<AbstractCharacterController>();
    }

    private void Update()
    {      
        CoolDown();
    }
 
    protected virtual void CoolDown()
    {
        if (!IsAttackAvaliable)
        {
            if (m_coolDownTimer >= shootCollDown)
            {
                IsAttackAvaliable = m_Char.Target != null;
            }
            else
            {
                m_coolDownTimer += Time.deltaTime;
            }
        }
    }

    public void ResetColldownTimer()
    {
        IsAttackAvaliable = false;
        m_coolDownTimer = 0;
    }

    // call from animation
    public virtual void AnimationAttack()
    {
        IsAttackAvaliable = false;
        m_coolDownTimer = 0;
        AttackWeapon();
    }


    protected void SendAttackSound()
    {
        //EventManager.SendEvent(attackSoundKey);
    }

    protected abstract void AttackWeapon();
}
