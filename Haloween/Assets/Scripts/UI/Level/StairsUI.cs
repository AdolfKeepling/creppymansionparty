﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;
using UnityEngine.UI;

public class StairsUI : MonoBehaviour
{
    private Text m_text;
    private float delay = 1.5f;
    public string startFloor;

    public CompositeDisposable disposables;
    void OnEnable()
    {
        disposables = new CompositeDisposable();
        MessageBroker.Default
            .Receive<EventManager>() // задаем тип MessageBase
            .Where(msg => msg.key == EventKeys.SHOW_FLOOR_TEXT)//фильтруем message по id
            .Subscribe(msg => { // подписываемся
                StartCoroutine(SetFloorNumber((string)msg.data));
            }).AddTo(disposables);

        MessageBroker.Default
            .Receive<EventManager>() // задаем тип MessageBase
            .Where(msg => msg.key == EventKeys.SHOW_FLOOR_TEXT_START)//фильтруем message по id
            .Subscribe(msg => { // подписываемся
                StartCoroutine(SetFloorNumber(startFloor));
            }).AddTo(disposables);

    }
    void OnDisable()
    { // отписываемся
        if (disposables != null)
        {
            disposables.Dispose();
        }
    }

    void Start()
    {
        m_text = GetComponent<Text>();
        m_text.enabled = false;
    }

    private IEnumerator SetFloorNumber(string value)
    {
        m_text.enabled = true;
        m_text.text = "FLOOR " + value;
        yield return new WaitForSeconds(delay);
        m_text.enabled = false;
    }
}
