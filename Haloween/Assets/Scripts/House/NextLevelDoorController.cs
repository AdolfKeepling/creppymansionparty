﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(SpriteRenderer))]
[RequireComponent(typeof(BoxCollider2D))]
public class NextLevelDoorController : AbstractUsableItemsController
{

    public string nextLevelName;
    protected override void ActionInput(GameObject player)
    {
        if (!isVisible) return;

        if (Vector2.Distance(player.transform.position, transform.position) < distToPlayer)
        {
            EventManager.SendEvent(EventKeys.LOAD_LEVEL_NAME, nextLevelName);
        }
    }
}
