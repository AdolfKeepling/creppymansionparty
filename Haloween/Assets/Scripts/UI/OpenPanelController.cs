﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;

public enum UIAppStates
{
   menu = 0,
   pause = 1,
   settings = 2,
   save = 3,
   load = 4,
   game = 5
}

public class OpenPanelController : MonoBehaviour {

    /// <summary>
    /// Current state
    /// </summary>
    private GameObject content;
    public UIAppStates state;

    public CompositeDisposable disposables;
    void OnEnable()
    {
        disposables = new CompositeDisposable();
        MessageBroker.Default
            .Receive<EventManager>() // задаем тип MessageBase
            .Where(msg => msg.key == EventKeys.UPDATE_UI_STATE)//фильтруем message по id
            .Subscribe(msg => { // подписываемся
                ChangeState((UIAppStates) msg.data);
            }).AddTo(disposables);
    }
    void OnDisable()
    { // отписываемся
        if (disposables != null)
        {
            disposables.Dispose();
        }
    }

    void Awake ()
    {
        content = transform.GetChild(0).gameObject;
    }

    private void ChangeState(UIAppStates _state)
    {
        if (state == _state)
        {
            Common.StateUI = _state;
            ActivatePanel();
        }
        else
        {
            content.SetActive(false);
        }
    }


    private void ActivatePanel()
    {      
        if (!content.activeSelf)
        {
            content.SetActive(true);
        }
    }
}
