﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AICharcterController : AbstractCharacterController
{

    public float attackRadius;
    public float chaseRadius;
    public float speed;
    protected List<Transform> m_movePoints = new List<Transform>();
    public Transform movePointsParent;

    /// <summary>
    /// move points index
    /// </summary>
    protected int m_mpIndex = 0;
    protected float m_distToPoint = 0.05f;
    protected bool m_isVisible = false;

    private GameObject m_target;
    protected CharacterAttackAbstract m_Attack;

    private Vector3 m_raycastOffset = new Vector3(0,0.1f,0);

    public override GameObject Target
    {   get => m_target;
        set
        {
            m_target = value;
            if (m_target != null)
            {
                LookAtTarget(m_target.transform);
                CheckDistanceToTarget();
            }
        }
    }

    protected void Start()
    {
        m_Attack = GetComponent<CharacterAttackAbstract>();
        GetMovePoints();
        State = AIState.patrol;
    }

    protected new void Update()
    {
        base.Update();
        AnimController();
    }

    protected void OnBecameVisible()
    {
        m_isVisible = true;
    }

    protected void OnBecameInvisible()
    {
        State = AIState.patrol;
        m_isVisible = false;
    }

    protected override void OnCollisionEnter2D(Collision2D _collision)
    {
        CollisionDamage(_collision.gameObject, m_Attack.damage);
    }

    protected virtual void AnimController()
    {
        m_Anim.SetBool("isWalk",IsMovement);
        m_Anim.SetBool("isAttack", m_Attack.IsAttackAvaliable);
    }

    protected virtual void GetMovePoints()
    {
        for (int i = 0; i < movePointsParent.childCount; i++)
            m_movePoints.Add(movePointsParent.GetChild(i));
    }

    private void LookingForPlayer()
    {
        if (!m_isVisible) return; 

        Raycast2D(Vector2.left);
        Raycast2D(Vector2.right);
    }
    private void Raycast2D(Vector2 _direction)
    {
        RaycastHit2D hit = Physics2D.Raycast(transform.position + m_raycastOffset, _direction, chaseRadius, m_Health.enemyLayer);

        // If it hits something...
        if (hit.collider != null)
        {
            if (hit.collider.GetComponent<PlayerController>() != null)
                Target = hit.collider.gameObject;
        }
    }

    protected void LookAtTarget(Transform _target)
    {
        if (_target.position.x > transform.position.x)
        {
            if ( transform.localScale.x > 0)
                Flip();
        }
        else
        if (_target.position.x < transform.position.x )
        {
            if ( transform.localScale.x < 0)
                Flip();
        }
    }

    protected virtual void Flip()
    {
        // Multiply the player's x local scale by -1.
        Vector3 theScale = transform.localScale;
        theScale.x *= -1;
        transform.localScale = theScale;

        speed *= -1;
    }

    protected virtual void CheckDistanceToTarget()
    {
        if (Vector2.Distance(transform.position, Target.transform.position) < attackRadius)
        {
            State = AIState.attack;

        }else
            State = AIState.chase;
    }

    protected virtual void FollowObject(GameObject _obj)
    {
        if (IsMovement)
        {
            if (_obj != null)
            {
                transform.position += Vector3.right * speed * Time.deltaTime;
            }
        }
    }

    protected virtual void WalkOnMovePoints()
    {
        if (Mathf.Abs(transform.position.x - m_movePoints[m_mpIndex].position.x) < m_distToPoint)
        {
            m_mpIndex = Common.UpdateSelectedIndexInList(m_mpIndex, m_movePoints.Count, true);
            LookAtTarget(m_movePoints[m_mpIndex]);
        }
        FollowObject(m_movePoints[m_mpIndex].gameObject);
    }

    #region STATES
    protected override void Attack()
    {
        if (Target.activeSelf)
        {
            CheckDistanceToTarget();
            LookAtTarget(Target.transform);
        }
        else
            State = AIState.patrol;
    }

    protected override void Chase()
    {
        FollowObject(Target);
        CheckDistanceToTarget();
    }

    protected override void Idle()
    {
        LookingForPlayer();
    }

    protected override void Patrol()
    {
        LookingForPlayer();
        WalkOnMovePoints();
    }
    #endregion ABSTRACT STATES

    #region ABSTRACT STATES IN
    protected override void IdleIN()
    {
        IsMovement = false;
    }

    protected override void PatrolIN()
    {
        IsMovement = true;
        Target = null;
        LookAtTarget(m_movePoints[m_mpIndex]);
    }

    protected override void AttackIN()
    {
        IsMovement = false;
    }

    protected override void ChaseIN()
    {
        IsMovement = true;
    }

    protected override void DieIN()
    {
        IsMovement = false;
    }
    #endregion ABSTRACT STATES IN


    #region ABSTRACT STATES OUT
    protected override void IdleOUT()
    {
        IsMovement = false;
    }

    protected override void PatrolOUT()
    {
        IsMovement = false;
    }

    protected override void AttackOUT()
    {
        IsMovement = false;
    }

    protected override void ChaseOUT()
    {
        IsMovement = false;
    }
    #endregion ABSTRACT STATES OUT
}
