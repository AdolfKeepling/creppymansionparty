﻿using UnityEngine;
using System.Collections;

public class InitHelpers : MonoBehaviour {

    public GameObject prefabHelper;

    void Awake()
    {
        InstantiatePrefab(Tags.Helper, prefabHelper);
    }
 
    void InstantiatePrefab(string tag, GameObject prefab)
    {
        if (GameObject.FindGameObjectWithTag(tag) == null)
        {
            DontDestroyOnLoad(Instantiate(prefab));
        }
       
    }

}
