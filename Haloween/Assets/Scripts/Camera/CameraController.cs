﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class CameraController : MonoBehaviour {


    public enum CameraMovement { follow , moveTo , standing } 

    private static CameraController _instance;
    public static CameraController Instance { get { return _instance; } }

    private Camera mainCamera;
    private Transform followingTarget;

    [Header("Shake")]
    public float duration = 0.5f;
    public float speed = 1.0f;
    public float magnitude = 0.2f;
    public bool test = false;

    [Header("Following")]
    public int distance = -10;
    public float lift = 0;
    public bool isFindDiver = true;
    private CameraMovement movementType;

    private float baseCameraSize;
    private float goalCameraSize;
    private float cameraSizeSpeed = 1f;

    private float GoalCameraSize
    {
        get { return mainCamera.orthographicSize; }
        set { goalCameraSize = value; }
    }

    private float moveSpeed = 0.3f;
    
    void Awake()
    {
        if (_instance == null)
            _instance = this;
    }

    void Start () {
        
        mainCamera = gameObject.GetComponent<Camera>();
        if (isFindDiver)
        {
            GameObject target = GameObject.FindGameObjectWithTag(Tags.Player);
            if (target!=null)
                InitFollowTarget(target, CameraMovement.follow);
        }
        baseCameraSize = mainCamera.orthographicSize;
        goalCameraSize = baseCameraSize;

      
    }
	

	void Update () {

        switch (movementType)
        {
            case CameraMovement.follow: { CameraFollow(); break; }
            case CameraMovement.moveTo: { MoveToTarget(); break; }
            case CameraMovement.standing: { break; }
        }

        ChangeCameraSize();

        if (test)
        {
            test = false;
            PlayShake();
        }

        //if (SceneHelper.Instance.currentScene == EnumScenes.Level10 || SceneHelper.Instance.currentScene == EnumScenes.Level26)
        //{
        //    ResetCameraSize();
        //}
    }

    public void ResetCameraSize()
    {
        transform.localPosition = new Vector3(0, 0, distance);
    }

    public void InitFollowTarget(GameObject target, CameraMovement type)
    {
        if (target != null)
        {
            movementType = type;
            followingTarget = target.transform;
        }
    }

    public void InitFollowTarget(GameObject target, CameraMovement type, float speed)
    {
        movementType = type;
        followingTarget = target.transform;
        moveSpeed = speed;
    }

    void CameraFollow()
    {
        if (followingTarget != null)
        {

            transform.parent.position = new Vector3(0, lift, -10) + followingTarget.position;
          //  transform.LookAt(followingTarget);
            
        }
    }

    void MoveToTarget()
    {
        if (followingTarget != null)
        {
            transform.parent.position = Vector2.MoveTowards(transform.parent.position, followingTarget.position, moveSpeed * Time.deltaTime);
          
        }
    }

    public void InitCameraSize(float size )
    {
        goalCameraSize = size;
        print(size);
    }
    public void InitCameraSize(float size , float cameraSizeSpeed)
    {
        goalCameraSize = size;
        this.cameraSizeSpeed = cameraSizeSpeed;
    }

    void ChangeCameraSize()
    {
        if (goalCameraSize != baseCameraSize)
        {
            if (goalCameraSize < baseCameraSize)
            {
                baseCameraSize -= Time.deltaTime * cameraSizeSpeed;
                mainCamera.orthographicSize = baseCameraSize;
                if (goalCameraSize > mainCamera.orthographicSize)
                {
                    print(goalCameraSize + "_1");
                    mainCamera.orthographicSize = goalCameraSize ;
                    baseCameraSize = goalCameraSize;

                }
            }

            if (goalCameraSize > baseCameraSize)
            {
                baseCameraSize += Time.deltaTime * cameraSizeSpeed;
                mainCamera.orthographicSize = baseCameraSize;
                if (goalCameraSize < mainCamera.orthographicSize)
                {
                    print(goalCameraSize + "_2");
                    mainCamera.orthographicSize = goalCameraSize ;
                    baseCameraSize = goalCameraSize;
                }

            }
        }
    }

    public void PlayShake()
    {

        StopAllCoroutines();
        StartCoroutine(Shake());
    }

    IEnumerator Shake()
    {

        float elapsed = 0.0f;

        Vector3 originalCamPos = mainCamera.transform.position;
        float randomStart = Random.Range(-500.0f, 500.0f);

        while (elapsed < duration)
        {

            elapsed += Time.deltaTime;

            float percentComplete = elapsed / duration;

            // We want to reduce the shake from full power to 0 starting half way through
            float damper = 1.0f - Mathf.Clamp(2.0f * percentComplete - 1.0f, 0.0f, 1.0f);

            // Calculate the noise parameter starting randomly and going as fast as speed allows
            float alpha = randomStart + speed * percentComplete;

            // map noise to [-1, 1]
            float x = Util.Noise.GetNoise(alpha, 0.0f, 0.0f) * 2.0f - 1.0f;
            float y = Util.Noise.GetNoise(0.0f, alpha, 0.0f) * 2.0f - 1.0f;



            x *= magnitude * damper;
            y *= magnitude * damper;

            mainCamera.transform.position = new Vector3(originalCamPos.x + x, originalCamPos.y +  y, originalCamPos.z);

            yield return null;
        }

        mainCamera.transform.position = originalCamPos;
    }
}
