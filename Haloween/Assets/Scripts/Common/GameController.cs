﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UniRx;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour {


    public static GameController instance;
    [Header("Gane Events")]
    public List<GameEvent> gameEvents;
    [Header("Start Events")]
    public List<StartEvents> startEvents;
    [Header("Players")]
    public List<HealthCharacter> playersHealthList;
    [Header("Scriptable")]
    public PlayerInputsHolder playerInputsHolder;
    public PlayersHolder playersHolder;

    // container to instantiate UI
    private Transform hpUIContainer;
    // start spawn point
    private Transform playerSpawnPoint;

    private bool _isGameOver = false;
    public bool IsGameOver
    {
        get => _isGameOver;
        set
        {
            _isGameOver = value;
            if (_isGameOver)
                EventManager.SendEvent(EventKeys.GAME_OVER);
        }
    }

    #region EVENT
    public CompositeDisposable myEvent;
    void OnEnable()
    {
        myEvent = new CompositeDisposable();
        // check game events
        MessageBroker.Default
           .Receive<EventManager>()
           .Where(msg => ContaintsEvent(msg.key))
           .Subscribe(msg => { }).AddTo(myEvent);

        // listen player died
        MessageBroker.Default
          .Receive<EventManager>()
          .Where(msg => msg.key == EventKeys.PLAYER_DIED)
          .Subscribe(msg => 
          {
              print("PLAYER_DIED");
              PlayerDied();
          }).AddTo(myEvent);
        // listen restart input
        MessageBroker.Default
        .Receive<EventManager>()
        .Where(msg => msg.key == EventKeys.PLAYER_PRESS_BUTTON)
        .Subscribe(msg =>
        {
            if ((string)msg.data == InputKeys.RESTART)
                RestartGame();
        }).AddTo(myEvent);
    }

    void OnDisable()
    {
        if (myEvent != null)
        {
            myEvent.Dispose();
        }
    }
    #endregion

    private void Awake()
    {
        instance = this;
        RestoreData();

        EventManager.SendEvent(EventKeys.UPDATE_UI_STATE, UIAppStates.game);
    }

    IEnumerator Start()
    {
        hpUIContainer = GameObject.FindGameObjectWithTag(Tags.hpUIHolder).transform;
        playerSpawnPoint = GameObject.FindGameObjectWithTag(Tags.Respawn).transform;
        SetAllPlayers();

        foreach (var x in startEvents)
        {
            if (!EventIsDone(x.eventKey))
            {
                yield return new WaitForSeconds(x.eventDelay);
                EventManager.SendEvent(x.eventKey);
            }
        }
    }

    private void RestartGame()
    {
        if (IsGameOver)
            EventManager.SendEvent(EventKeys.RELOAD_LEVEL);
    }

    private bool ContainsValue(string key)
    {
        return gameEvents.Single(x => x.eventKey == key) != null;
    }

    protected bool ContaintsEvent(string key)
    {
        if (gameEvents.Count == 0) return false;

        bool value = false;

        foreach (var x in gameEvents)
        {
            if (x.eventKey == key)
            {
                x.isComplated = true;
                break;
            }
        }

        return value;
    }

    public bool EventIsDone(string eventName)
    {
        try
        {
            return gameEvents.Single(x => x.eventKey == eventName).isComplated;
        }
        catch
        {
            return false;
        }
    }

    public void RestoreData()
    {
        if (!Common.IsRestoreFromStorage) return;

        for (int i = 0; i < gameEvents.Count; i++)
        {
            if (gameEvents[i].eventKey == Common.saveData.gameEvents[i].eventKey)
            {
                gameEvents[i].isComplated = Common.saveData.gameEvents[i].isComplated;
            }
        }
    }

    public void SetEvent(string eventKey, bool value)
    {
        try
        {
            gameEvents.Single(x => x.eventKey == eventKey).isComplated = value;
        }
        catch
        {
            print("Event not found");
        }
    }

    private void SetAllPlayers()
    {     
        for (int i = 0; i < playersHolder.players.Count; i++)
        {
            GameObject pl = Instantiate(playersHolder.players[i].playerPrefab,playerSpawnPoint.position,Quaternion.identity);
            GameObject ui = Instantiate(playersHolder.players[i].playerUIPrefab, hpUIContainer);
            SetUpPlayersInputs(pl,playerInputsHolder.playerInputs[i]);
            SetUpPlayersUI(pl,ui);
            playersHealthList.Add(pl.GetComponent<HealthCharacter>());
        }
    }

    private void SetUpPlayersInputs(GameObject player, PlayerInput input)
    {
        player.GetComponent<PlayerInputController>().InitInput(input);
    }

    private void SetUpPlayersUI(GameObject player, GameObject ui)
    {
        ui.GetComponent<HealthBar>().InitHelathBar(player);
        player.GetComponent<BuffableEntity>().HealthBar = ui.GetComponent<HealthBar>();
    }

    private void PlayerDied()
    {
        bool isAllDied = true;
        foreach (HealthCharacter x in playersHealthList)
        {
            isAllDied &= !x.IsAllive;
        }
        IsGameOver = isAllDied;
    }
}
