﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleEnemie : AbstractCharacterController
{
    public int damage;
    public float speed;
    public Transform mPointsParent;
    private List<Transform> movePoints = new List<Transform>();
    private Transform m_target;
    private int m_index = 1;
    bool isLoop = true;

    // Start is called before the first frame update
    void Start()
    {
        GetMovePoints();
        State = AIState.patrol;
        m_target = movePoints[m_index];
        transform.position = m_target.position;
    }

    private void GetMovePoints()
    {
        for(int i=0;i<mPointsParent.childCount;i++)
            movePoints.Add(mPointsParent.GetChild(i));
    }

    private void MoveLoop()
    {
        float step = speed * Time.deltaTime; // calculate distance to move
        transform.position = Vector2.MoveTowards(transform.position, m_target.position, step);

        // Check if the position of the cube and sphere are approximately equal.
        if (Vector2.Distance(transform.position, m_target.position) < 0.001f)
        {
            if (m_index == movePoints.Count - 1 || m_index == 0)
                isLoop = !isLoop;
            m_index = Common.UpdateSelectedIndexInList(m_index, movePoints.Count, isLoop);
            m_target = movePoints[m_index];
        }
    }

    protected override void OnTriggerEnter2D(Collider2D collider)
    {
        CollisionDamage(collider.gameObject, damage);
    }

    #region ABSTRACT STATES 
    protected override void Attack()
    {
    }

    protected override void Chase()
    {
    }

    protected override void Patrol()
    {
        MoveLoop();
    }

    protected override void Idle()
    {
    }
    #endregion ABSTRACT STATES 

    #region ABSTRACT STATES IN
    protected override void IdleIN()
    {
    }

    protected override void PatrolIN()
    {
    }

    protected override void AttackIN()
    {
    }

    protected override void ChaseIN()
    {
    }

    protected override void DieIN()
    {
    }
    #endregion ABSTRACT STATES IN

    #region ABSTRACT STATES OUT
    protected override void IdleOUT()
    {
    }

    protected override void PatrolOUT()
    {
    }

    protected override void AttackOUT()
    {
    }

    protected override void ChaseOUT()
    {
    }
    #endregion ABSTRACT STATES OUT
}
