﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class SelectorButton : MonoBehaviour
{
    [SerializeField] string selectorName;
    [SerializeField] Text selectorTxt;
    [SerializeField] Image selectorImg;

    bool isSelected;
    public bool IsSelected
    {
        get => isSelected;
        set
        {
            isSelected = value;
            selectorImg.enabled = isSelected;
        }
    }


    [SerializeField] UnityEvent submitEvent;
    [SerializeField] UnityEvent moveLeftEvent;
    [SerializeField] UnityEvent moveRightEvent;

    public UnityEvent MoveLeftEvent
    {
        get => moveLeftEvent;
        set => moveLeftEvent = value;
    }
    public UnityEvent MoveRightEvent
    {
        get => moveRightEvent;
        set => moveRightEvent = value;
    }
    public UnityEvent SubmitEvent
    {
        get => submitEvent;
        set => submitEvent = value;
    }

    private void Start()
    {
        selectorTxt.text = selectorName;
    }
}
