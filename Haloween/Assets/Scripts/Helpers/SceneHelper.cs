﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;
using UnityEngine.SceneManagement;

public class SceneHelper : MonoBehaviour {

    private string menuSceneKey = "Menu";

    #region EVENTS
    public CompositeDisposable loadSceneEvents;
    void OnEnable()
    {
        loadSceneEvents = new CompositeDisposable();
        MessageBroker.Default
            .Receive<EventManager>() 
            .Where(msg => msg.key == EventKeys.LOAD_LEVEL_NAME)
            .Subscribe(msg => {
                LoadLevel((string)msg.data);
            }).AddTo(loadSceneEvents);

        MessageBroker.Default
          .Receive<EventManager>()
          .Where(msg => msg.key == EventKeys.LOAD_LEVEL_NUM)
          .Subscribe(msg => {
              LoadLevel((int)msg.data);
          }).AddTo(loadSceneEvents);

        MessageBroker.Default
           .Receive<EventManager>()
           .Where(msg => msg.key == EventKeys.LOAD_PREV_LEVEL)
           .Subscribe(msg => {
               LoadPrevLevel();
           }).AddTo(loadSceneEvents);

        MessageBroker.Default
           .Receive<EventManager>() 
           .Where(msg => msg.key == EventKeys.LOAD_NEXT_LEVEL)
           .Subscribe(msg => {
               LoadNextLevel();
            }).AddTo(loadSceneEvents);

        MessageBroker.Default
          .Receive<EventManager>()
          .Where(msg => msg.key == EventKeys.LOAD_NEXT_LEVEL_DELAY)
          .Subscribe(msg => {
              StartCoroutine(LoadNextLevelDelay());
          }).AddTo(loadSceneEvents);

        MessageBroker.Default
           .Receive<EventManager>() 
           .Where(msg => msg.key == EventKeys.RELOAD_LEVEL)
           .Subscribe(msg => {
               ReloadLevel();
            }).AddTo(loadSceneEvents);

        MessageBroker.Default
         .Receive<EventManager>()
         .Where(msg => msg.key == EventKeys.GO_TO_MENU)
         .Subscribe(msg => {
             LoadLevel(menuSceneKey);
         }).AddTo(loadSceneEvents);
    }

    void OnDisable()
    {
        if (loadSceneEvents != null)
        {
            loadSceneEvents.Dispose();
        }
    }
    #endregion

    private void Start()
    {
        InitCurrentScene();
    }

    private void InitCurrentScene()
    {
        Common.currentLevelName = SceneManager.GetActiveScene().name;
        Common.currentLevelIndex = SceneManager.GetActiveScene().buildIndex;
    }

    private void LoadLevel(string levelName)
    {
        print("LoadLevel " + levelName);
        SceneManager.LoadScene(levelName);
        InitCurrentScene();
    }

    private void LoadLevel(int levelNum)
    {
        print("LoadLevel " + levelNum);
        SceneManager.LoadScene(levelNum);
        InitCurrentScene();
    }

    private void ReloadLevel()
    {
        print("ReloadLevel");
        LoadLevel(Common.currentLevelName);
        InitCurrentScene();
    }

    private IEnumerator LoadNextLevelDelay(float delay = 1.5f)
    {
        print("LoadNextLevel");
        yield return new WaitForSeconds(delay);
        LoadNextLevel();
    }

    private void LoadPrevLevel()
    {
        print("LoadPrevLevel");
        int levelIndex = SceneManager.GetActiveScene().buildIndex;
        levelIndex = Common.UpdateSelectedIndexInList(levelIndex, SceneManager.sceneCountInBuildSettings, false);
        SceneManager.LoadScene(levelIndex);
        InitCurrentScene();
    }

    private void LoadNextLevel()
    {
        print("LoadNextLevel");
        int levelIndex = SceneManager.GetActiveScene().buildIndex;
        levelIndex =  Common.UpdateSelectedIndexInList(levelIndex , SceneManager.sceneCountInBuildSettings , true);
        SceneManager.LoadScene(levelIndex);
        InitCurrentScene();
    }
}
