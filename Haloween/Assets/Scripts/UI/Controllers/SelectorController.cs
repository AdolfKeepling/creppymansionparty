﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class SelectorController : MonoBehaviour
{

    [SerializeField] List<SelectorButton> selectors;

    [SerializeField] int currentSelector;
    public int CurrentSelector
    {
        get => currentSelector;
        set
        {
            currentSelector = value;
            RefreshSelectors();
        }
    } 

    void Start()
    {
        selectors = GetComponentsInChildren<SelectorButton>().ToList();
        CurrentSelector = 0;
    }

    void RefreshSelectors()
    {
        selectors.ForEach(x=> x.IsSelected = false);
        selectors[CurrentSelector].IsSelected = true;
    }

    public void MoveUp()
    {
        CurrentSelector = Common.UpdateSelectedIndexInList(CurrentSelector, selectors.Count,false);
    }

    public void MoveDown()
    {
        CurrentSelector = Common.UpdateSelectedIndexInList(CurrentSelector, selectors.Count, true);
    }

    public void MoveRight()
    {
        selectors[CurrentSelector].MoveRightEvent?.Invoke();
    }

    public void MoveLeft()
    {
        selectors[CurrentSelector].MoveLeftEvent?.Invoke();
    }

    public void Submit()
    {
        selectors[CurrentSelector].SubmitEvent?.Invoke();
    }
}
