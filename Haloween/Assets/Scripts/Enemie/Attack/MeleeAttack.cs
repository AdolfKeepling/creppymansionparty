﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeleeAttack : CharacterAttackAbstract
{
   

    public Transform attackPoint;



    //CHECK THIS DISTANCE - attackRadius

    protected override void AttackWeapon()
    {
        switch (splashAttackType)
        {
            case MeleeAttackType.splash: { SplashMeleeAttack();break; }
            case MeleeAttackType.target: { TargetAttack(); break; }
            case MeleeAttackType.mixed: { TargetAttack(); SplashMeleeAttack(); break; }
        }
        SendAttackSound();
    }


    protected void SplashMeleeAttack()
    {
        slashDelegate?.Invoke();

        Collider2D[] meleeHitColl =
        // Physics.OverlapCapsule(bulletSpawnPosition.position, meleeRadius, m_Health.enemyLayer.value);//
        Physics2D.OverlapCircleAll(attackPoint.position, raycastSplashRadius, m_Health.enemyLayer);
        foreach (Collider2D col in meleeHitColl)
        {
            if (col != null)
            {
                col.gameObject.GetComponent<Health>().TakeDamage(damage, gameObject);
            }
        }
    }

    protected void TargetAttack()
    {
        targetDelegate?.Invoke();
    }
}
