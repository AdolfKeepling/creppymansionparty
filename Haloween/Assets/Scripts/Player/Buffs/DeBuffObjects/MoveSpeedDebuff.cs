﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveSpeedDebuff : AbstractDeBuff
{
    public override void DoDebuff(GameObject target)
    {
        TimedBuff speedBuff = new TimedMoveSpeedBuff(scriptableBuff, target);
        target.GetComponent<BuffableEntity>().AddBuff(speedBuff);
    }
}